# installation
- extract `chat-x.x.x.zip`
- start the server running `run-server.bat` or `run-server.sh`
- the server will start with `Servidor iniciado.` message
- start the client app running `run.bat` or `run.sh`
- the client will be started
- open other client to can test the chat

# license
Copyright [2015] [Elvis de Freitas]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

# suggestions and disclaimers
- please send disclaimers/suggestions to edigitalb@gmail.com or create it on [our repository](https://bitbucket.org/ElvisdeFreitas/mageddo-chat)

# installation of clients in others machines
- configure server ip on `conf/config.conf`

# installation on linux

	$ unzip chat-x.x.x.zip
	$ cd chat
	$ sh run-server.sh 
	INFO 2015-08-14 22:20:52 (Logger.java:13) - Iniciando o servidor...
	INFO 2015-08-14 22:20:52 (Logger.java:13) - Servidor iniciado.
	$ sh run.sh
