package other;

import java.awt.event.*;  
import javax.swing.*;  
   
public class BlinkFrame extends JFrame implements Runnable {  
   
    private Thread t;  
    private boolean stopThread;  
    private boolean gotMessage;  
    protected JTextArea center;  
   
    public BlinkFrame() {  
        super( "Blink Frame" );  
        addWindowListener( new WindowAdapter() {  
            public void windowActivated( WindowEvent e ) {  
                gotMessage = false;  
                stopThread = true;  
                toFront();  
                
            }  
        } );  
        center = new JTextArea();  
        JScrollPane scroll = new JScrollPane( center );  
        getContentPane().add( scroll );  
        t = new Thread( this );  
        t.start();  
    }  
   
    public void run() {  
        while( true ) {  
            if( gotMessage && getState() == ICONIFIED ) {  
                stopThread = false;  
                while( !stopThread ) {  
                    try {  
                        toBack();  
                        Thread.sleep( 100 );  
                        toFront();  
                        Thread.sleep( 100 );  
                    }  
                    catch( InterruptedException x ) {  
                        x.printStackTrace();  
                    }  
                }  
            }  
            try {  
                Thread.sleep( 100 );  
            }  
            catch( InterruptedException x ) {  
                x.printStackTrace();  
            }  
        }  
    }  
   
    public void sendMessage() {  
        if( getState() == ICONIFIED ) {  
            gotMessage = true;  
        }  
        center.append( "Got New Message!\n" );  
        System.out.println( "Got New Message!" );  
    }  
   
    public static void main( String[] arg ) {  
        final BlinkFrame f = new BlinkFrame();  
        f.setSize( 300, 300 );  
        f.setDefaultCloseOperation( EXIT_ON_CLOSE );  
        f.setVisible( true );  
   
        Thread messageThread = new Thread() {  
            public void run() {  
                while( true ) {  
                    try {  
                        Thread.sleep( 10000 );  
                        f.sendMessage();  
                    }  
                    catch( InterruptedException x ) {  
                        x.printStackTrace();  
                    }  
                }  
            }  
        };  
        messageThread.start();  
    }  
}  