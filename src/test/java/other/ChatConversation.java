package other;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;

public class ChatConversation {
	private StringBuilder str;
	private InputStream in;
	private OutputStream out;

	public ChatConversation(InputStream in, OutputStream out) {
		this.in = in;
		this.out = out;
		str = new StringBuilder();
	}
	
	public String readLine() throws IOException {
		str.delete(0, str.length());

		int i;
		while ((i = in.read()) != -1) {
			if(i == 10){
				break;
			}else{
				str.append((char) i);
			}
		}

		return str.toString();

	}

	/**
	 * Escreve String e finaliza com \n
	 * @param str
	 * @throws IOException
	 */
	public void writeLine(String str) throws IOException {
		str += "\n";
		byte[] bytes = str.getBytes();
		out.write(bytes, 0, bytes.length);
		out.flush();
	}
	
	
	/**
	 * Escreve o arquivo para o output stream e faz um flush
	 */
	public void writeToOut(byte[] data) throws IOException{
		out.write(data, 0, data.length);
		out.flush();
	}
		
	/**
	 * Lê os dados de um inputStream de socket para o array de bytes
	 */
	public byte[] lerBytes(byte[] bytes) throws IOException{
		int bytesLidos;
		int corrente = 0;
		
		do {
			bytesLidos = in.read(bytes, corrente, bytes.length - corrente);
			if (bytesLidos != -1)
				corrente += bytesLidos;
		} while (bytesLidos != bytes.length);
		
		return bytes;
	}
	
	/**
	 * Retorna os bytes de um arquivo
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public byte[] getFileBytes(File file) throws IOException{
		FileInputStream in = new FileInputStream(file);
		byte[] array =  IOUtils.toByteArray(in);
		in.close();
		return array;
	}
	
	
}
