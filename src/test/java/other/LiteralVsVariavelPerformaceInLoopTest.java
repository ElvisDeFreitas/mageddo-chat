package other;

public class LiteralVsVariavelPerformaceInLoopTest {

	public static void main(String[] args) {
		
		long now = 0;
		long last1;
		long last2;
		int length = 10000;
		now = System.nanoTime();
		for (int i = 0; i < length; i++) {
			sum(2);
		}
		last1 = System.nanoTime() - now;
		
		int number = 2;
		now = System.nanoTime();
		for (int i = 0; i < length; i++) {
			sum(number);
		}
		last2 = System.nanoTime() - now;
		
		System.out.println(last1 - last2);

	}
	
	public static void sum(int val){
		int i = val * 2;
	}

}
