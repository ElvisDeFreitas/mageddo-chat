package other;

public class ExcecaoTest {
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Integer quatro = null;
		try{
			quatro += 1;
		}catch(Exception e){
			System.out.println(e.getMessage());
			System.out.println(e);
			for (StackTraceElement o : e.getStackTrace()) {
				System.out.println(o);
			};
		}
	}
}
