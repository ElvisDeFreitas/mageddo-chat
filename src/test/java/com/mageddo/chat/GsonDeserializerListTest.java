package com.mageddo.chat;

import java.lang.reflect.Type;
import java.util.List;

import javax.naming.directory.SearchResult;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mageddo.chat.bean.Message;

public class GsonDeserializerListTest {

	public static void main(String[] args) {
		String json = "[{\"to\":[\"m2\"],\"from\":\"ELVIS - m2\",\"message\":\"dadasd\n\",\"data\":{\"year\":2014,\"month\":6,\"dayOfMonth\":8,\"hourOfDay\":0,\"minute\":11,\"second\":18}}]";

		System.out.println(json);
		
		Type t = new TypeToken<List<Message>>() {}.getType();
		List<Message> list = (List<Message>) new Gson().fromJson(json, t);

		for (Message r : list) {
		  System.out.println(r);
		}
		
	}

}
