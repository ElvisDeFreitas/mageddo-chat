package com.mageddo.chat;

import com.google.gson.Gson;
import com.mageddo.chat.bean.Message;
import com.mageddo.chat.bean.MessageText;

public class SerializWithGsonTest {

	public SerializWithGsonTest() {
		
	}

	public static void main(String[] args) {
		Message msg = new MessageText("Ola para todos");
		msg.add("joao");
		msg.add("maria");
		String res = new Gson().toJson(msg);
		
		System.out.println(res);
		
		System.out.println(new Gson().fromJson(res, Message.class).getTo());
	}
}
