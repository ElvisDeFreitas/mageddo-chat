package com.mageddo.chat;


/*
 * SimpleTableDemo.java requires no other files.
 */

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class Test2 extends JFrame {

	public JPanel painel = new JPanel(new GridLayout(1, 0));
	
	public Test2() {
		configure();
	}
	
    private void configure() {

        String[] columnNames = {"Usuários"};

        Object[][] data = {
        		{"ola"},
        		{"pra voce"},
        };

        final JTable table = new JTable(data, columnNames);
        table.setPreferredScrollableViewportSize(new Dimension(500, 70));
        table.setFillsViewportHeight(true);


        table.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                printDebugData(table);
            }
        });


        //Create the scroll pane and add the table to it.
        JScrollPane scrollPane = new JScrollPane(table);

        //Add the scroll pane to this panel.
        painel.add(scrollPane);
		
	}

    private void printDebugData(JTable table) {
        int numRows = table.getRowCount();
        int numCols = table.getColumnCount();
        javax.swing.table.TableModel model = table.getModel();

        System.out.println("Value of data: ");
        for (int i=0; i < numRows; i++) {
            System.out.print("    row " + i + ":");
            for (int j=0; j < numCols; j++) {
                System.out.print("  " + model.getValueAt(i, j));
            }
            System.out.println();
        }
        System.out.println("--------------------------");
    }


    
	public static void main(String[] args) {
		Test2 frame = new Test2();
		frame.painel.setOpaque(true);
		frame.setContentPane(frame.painel);
		frame.setSize(450, 200);
		frame.setVisible(true);
    }
}