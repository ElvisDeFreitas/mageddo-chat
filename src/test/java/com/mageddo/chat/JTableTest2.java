package com.mageddo.chat;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.naming.ldap.StartTlsRequest;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class JTableTest2 extends JFrame {
	JTable table;
	DefaultTableModel model;
	JScrollPane scroll;

	public JTableTest2() {
		start();
	}

	public void start() {

		String[] nomes = { "Nomes" };
		String[][] dados = { { "Elvis" }, { "Joao" }, { "Monica" } };
		model = new DefaultTableModel(dados, nomes);
		table = new JTable(model);
		scroll = new JScrollPane(table);

		
		
		for (int i = 0; i < 30; i++) {
			model.addRow(dados[1]);
			
		}
		model.removeRow(0);
		table.getCellRect(1, 0, false);
		
		add(scroll, BorderLayout.CENTER);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(300, 300);

	}

	public static void main(String[] args) {
		JTableTest2 frame = new JTableTest2();
		frame.start();
		frame.setVisible(true);
	}

}
