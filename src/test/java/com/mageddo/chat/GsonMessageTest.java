package com.mageddo.chat;

import com.google.gson.Gson;
import com.mageddo.chat.bean.MessageText;
import com.mageddo.chat.bean.RequestPath;

public class GsonMessageTest {

	public static void main(String[] args) {
		RequestPath req = new RequestPath();
		MessageText msg = new MessageText();
		msg.setMessage("Ola");
		req.setMsg(msg);
		
		String s = new Gson().toJson(req);
		System.out.println(s);
		
		System.out.println(new Gson().fromJson(s, RequestPath.class));
		
	}

}
