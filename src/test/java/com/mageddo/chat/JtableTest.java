package com.mageddo.chat;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.naming.ldap.StartTlsRequest;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class JTableTest extends JFrame {
	JTable table;
	DefaultTableModel model;
	JScrollPane scroll;

	public JTableTest() {
		start();
	}

	public void start() {

		String[] nomes = { "Nomes" };
		String[][] dados = { { "Elvis" }, { "Joao" }, { "Monica" } };
		model = new DefaultTableModel(dados, nomes);
		table = new JTable(model);
		scroll = new JScrollPane(table);

		
		// adicionando outras coisas
		model.addRow(dados[1]);
		model.removeRow(0);
		table.getCellRect(1, 0, false);
		
		add(scroll, BorderLayout.CENTER);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(300, 300);

	}

	public static void main(String[] args) {
		JTableTest frame = new JTableTest();
		frame.start();
		frame.setVisible(true);
	}

}
