package com.mageddo.chat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ListOrderTest {

	public static void main(String[] args) {
		List listaAntiga = new ArrayList();
		List listaNova= new ArrayList();
		
		listaAntiga.add("antiga - a");
		listaAntiga.add("antiga - b");
		
		
		listaNova.add("nova - a");
		listaNova.add("nova - b");
		
		listaNova.addAll(0, listaAntiga);
		
		System.out.println(Arrays.toString(listaNova.toArray()));

		
	}

}
