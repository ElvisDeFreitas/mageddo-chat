package com.mageddo.chat;

import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class JtableMouseTest extends JFrame {
	JTable table;
	DefaultTableModel model;
	JScrollPane scroll;

	public JtableMouseTest() {
		start();
	}

	public void start() {

		String[] nomes = { "Nomes" };
		String[][] dados = { { "Elvis" }, { "Joao" }, { "Monica" } };
		model = new DefaultTableModel(dados, nomes);
		table = new JTable(model){
			
			@Override
			protected void processMouseEvent(MouseEvent e) {
			
				if(e.getID() == MouseEvent.MOUSE_RELEASED){
					int i = getSelectedRowCount();
					getSelectedRow();
				} 
				super.processMouseEvent(e);
			}
		};
		scroll = new JScrollPane(table);

		
		
		add(scroll, BorderLayout.CENTER);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(300, 300);

	}

	public static void main(String[] args) {
		JtableMouseTest frame = new JtableMouseTest();
		frame.start();
		frame.setVisible(true);
	}

}
