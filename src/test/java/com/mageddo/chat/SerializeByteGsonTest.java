package com.mageddo.chat;

import com.google.gson.Gson;

public class SerializeByteGsonTest {
	
	public static void main(String[] args) {
		byte[] bytes = {1,2,34,5};
		
		String bytesJSON = new Gson().toJson(bytes);
		
		System.out.println(bytesJSON);
		
	}
	
}
