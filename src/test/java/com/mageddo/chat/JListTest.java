package com.mageddo.chat;

import java.awt.BorderLayout;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

public class JListTest extends JFrame{
	private static final long serialVersionUID = 1L;
	private DefaultListModel listModel;
	private JList list;
	public JListTest() {
		listModel = new DefaultListModel();
	    listModel.addElement("Jane Doe");
	    listModel.addElement("John Smith");
	    listModel.addElement("Kathy Green");

	    //Create the list and put it in a scroll pane.
	    list = new JList(listModel);
	    list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	    list.setSelectedIndex(0);
	    list.setVisibleRowCount(5);

		JScrollPane listScrollPane = new JScrollPane(list);
		
		//setLayout(new BorderLayout());
		add(listScrollPane);
		setSize(300, 200);
		setVisible(true);
	}
	
	public static void main(String[] args) {
		new JListTest();
	} 
}
