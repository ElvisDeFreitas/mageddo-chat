package com.mageddo.chat;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class JTableUsage2 extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	DefaultTableModel model;
	JTable table;
	String col[] = { "Name"};
	JButton botao = new JButton("add");
	JButton botaoRemove = new JButton("Remove");
	int index = 0;

	public static void main(String args[]) {
		new JTableUsage2().start();
	}

	public void start() {

		model = new DefaultTableModel(col, 30);
		table = new JTable(model) {
			@Override
			public boolean isCellEditable(int arg0, int arg1) {

				return false;
			}
		};
		JScrollPane pane = new JScrollPane(table);

		
		
		botao.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				table.setValueAt("csanuragjain", index++, 0);
				
				
			}
		});
		
		botaoRemove.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				table.setValueAt("", index--, 0);
				
				
			}
		});
		

		

		add(pane);
		add(botao);
		add(botaoRemove);
		setVisible(true);
		setSize(500, 400);
		setLayout(new FlowLayout());
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
}
