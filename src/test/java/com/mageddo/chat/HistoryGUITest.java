package com.mageddo.chat;

import javax.swing.JFrame;

import com.mageddo.chat.file.ChatPath;
import com.mageddo.chat.gui.HistoryGUI;

public class HistoryGUITest {

	public static void main(String[] args) {
		ChatPath path = new ChatPath("m2");
		JFrame frame = new HistoryGUI(path);
		frame.setSize(400, 300);
		frame.setVisible(true);

	}

}
