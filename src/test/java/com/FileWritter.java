package com;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FileWritter {

	/**
	 * Escreve um arquivo de texto simples
	 * @param file
	 * @param content
	 * @throws IOException
	 */
	public static void writeTextFile(File file, String content) throws IOException{

		if (!file.exists()) {
			file.createNewFile();
		}

		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write(content);
		bw.close();


	}


	
	/**
	 * Lê um arquivo de texto
	 * @param file
	 * @return String
	 * @throws IOException
	 */
	public static String readTextFile(File file) throws IOException{
		BufferedReader reader = new BufferedReader(new FileReader(file.getAbsoluteFile()));
		StringBuilder str = new StringBuilder();
		String chars;
		while((chars = reader.readLine()) != null){
			str.append(chars);
		}
		return str.toString();
	}
	
	/**
	 * Lista arquivos de um diretório
	 * @param folder
	 * @return List - lista de itens da pasta
	 */
	public static List listarArquivos(final File folder, boolean recursive, boolean onlyFiles, List arquivos) {
		if(folder.listFiles() == null)
			return arquivos;
		
	    for (final File fileEntry : folder.listFiles()) {
	        if (fileEntry.isDirectory()) {
	        	if(recursive)
	        		listarArquivos(fileEntry, recursive, onlyFiles, arquivos);
	        	if(!onlyFiles)
	        		arquivos.add(fileEntry.getName());
	        } else {
	        	arquivos.add(fileEntry.getName());
	        }
	    }
	    return arquivos;
	}
	
	/**
	 * Escreve o arquivo
	 */
	public static void writeFile(InputStream in, File file) throws IOException{
		FileOutputStream out = new FileOutputStream(file);
		writeFile(in, out);
		out.close();
	}
	
	public static void writeFile(File file , OutputStream out) throws IOException{
		FileInputStream in = new FileInputStream(file);
		writeFile(in, out);
		in.close();
	}
	
	/**
	 * Lê o input stream e joga no outputStream
	 * @param in
	 * @param out
	 * @throws IOException
	 */
	public static void writeFile(InputStream in, OutputStream out) throws IOException{
		int b;
		while((b = in.read()) != -1){
			out.write(b);
		}
		out.flush();
	}

	
	/**
	 * Escreve o arquivo para o output stream e faz um flush
	 */
	public static void writeToOut(byte[] data, OutputStream out) throws IOException{
		out.write(data, 0, data.length);
		out.flush();
	}
	
	/**
	 * Escreve o arquivo e faz o flush 
	 */
	public static void writeFile(byte[] data, File file) throws IOException{
		FileOutputStream w = new FileOutputStream(file);
		w.write(data);
		w.flush();
		w.close();
	}
	
	/**
	 * Lista arquivos de um diretório recursivamente e mostrando também as pastas
	 * @param folder
	 * @return List
	 */
	public static List listarArquivos(File folder){
		return listarArquivos(folder, true, false, new ArrayList());
	}
	
	/**
	 * Lista arquivos de um diretório mostrando também as pastas
	 * @param folder
	 * @return List
	 */
	public static List listarArquivos(File folder, boolean recursive){
		return listarArquivos(folder, recursive, false, new ArrayList());
	}
	
	/**
	 * Lista arquivos de um diretório 
	 * @param folder
	 * @return List
	 */
	public static List listarArquivos(File folder, boolean recursive, boolean onlyFiles){
		return listarArquivos(folder, recursive, onlyFiles, new ArrayList());
	}
	

	/**
	 * Lê os dados de um arquivo até o limite imposto
	 * @param file arquivo a ser liddo
	 * @param in inputStream de dados
	 * @param limite tamanho máximo permitido do arquivo
	 * @return byte[] array de bytes do arquivo
	 * @throws IOException
	 */
	public static byte[] lerArquivo(InputStream in, int limite) throws IOException{
		byte[] bytes = new byte[limite];
		int bytesLidos = in.read(bytes, 0, bytes.length);
		int corrente = bytesLidos;
		
		do {
			bytesLidos = in.read(bytes, corrente, bytes.length - corrente);
			if (bytesLidos != -1)
				corrente += bytesLidos;
		} while (bytesLidos != -1);

		return Arrays.copyOf(bytes, corrente);
	}


	
	/**
	 * Lê os dados de um inputStream de socket para o array de bytes
	 */
	public static byte[] ler2Byte(InputStream in, byte[] bytes) throws IOException{
		int bytesLidos;
		int corrente = 0;
		
		do {
			bytesLidos = in.read(bytes, corrente, bytes.length - corrente);
			if (bytesLidos != -1)
				corrente += bytesLidos;
		} while (bytesLidos != bytes.length || bytesLidos < bytes.length - corrente);
		
		return bytes;
	}
	
}
