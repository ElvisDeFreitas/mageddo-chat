package com.mageddo.chat.bean;

public class MessageFile extends Message{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8642027781645081335L;
	private String fileName;
	private long length;

	public MessageFile() {
		super();
	}
	
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public void setLength(long length) {
		this.length = length;
	}
	
	public long length() {
		return length;
	}

}
