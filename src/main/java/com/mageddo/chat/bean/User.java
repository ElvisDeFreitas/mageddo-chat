package com.mageddo.chat.bean;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.mageddo.chat.socket.ChatConversation;

public class User extends ChatConversation {

	private String nick;
	private Integer version;

	public User(InputStream in, OutputStream out) {
		super(in, out);
	}

	public User(String nick, InputStream in, OutputStream out) {
		super(in, out);
		this.nick = nick;
	}

	public void close() {
		try {
			getOut().close();
			getIn().close();
		} catch (IOException e) {
		}
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

}
