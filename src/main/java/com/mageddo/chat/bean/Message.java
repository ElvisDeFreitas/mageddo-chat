package com.mageddo.chat.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Message implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7080764525022281770L;
	private List<String> to;
	private String from;
	private Calendar data;
	
	public Message() {
		to = new ArrayList<String>();
	}


	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public List<String> getTo() {
		return to;
	}

	/**
	 * Adiciona um destinatario
	 * @param client
	 */
	public void add(String client) {
		to.add(client);
	}

	public Calendar getData() {
		return data;
	}

	public void setData(Calendar data) {
		this.data = data;
	}
	
}
