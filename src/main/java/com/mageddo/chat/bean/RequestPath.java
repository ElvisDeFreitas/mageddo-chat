package com.mageddo.chat.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Objeto de tráfico das requisições e respostas
 * @author m2_user
 */
public class RequestPath implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3749686509927634921L;
	public static final int SELECAO_SERVIDOR = 1;
	public static final int LOGIN = 2;
	public static final int LOGIN_ACEITO = 3;
	public static final int LOGIN_NEGADO = 4;
	public static final int MESSAGEM = 5;
	public static final int USERS = 6;
	public static final int KILL_IN_SERVER = 7;
	public static final int DISCONNECT = 8;
	public static final int MENSAGEM_PARA_CLIENTE = 9;
	public static final int REQUISICAO_VERSAO = 10;
	public static final int LOGIN_MANUAL = 11;
	public static final int ARQUIVO = 12;
	public static final int ARQUIVO_ACEITO = 13;
	public static final int ARQUIVO_NEGADO = 14;
	public static final int NOTIFICA_NOVA_VERSAO = 15;
	public static final int ATUALIZACAO_NOVA_VERSAO_ACEITA = 16;
	public static final int NOTIFICA_NOVA_VERSAO_REQUERIDA = 17;
	public static final int ATUALIZACAO_NOVA_VERSAO_NEGADA = 18;
	
	/**
	 * Tipo da instancia atual
	 */
	private int tipo = 1; 
	private MessageText msg;
	private List<String> users;
	private MessageFile file;

	public RequestPath() {

	}

	public RequestPath(MessageText msg) {
		this.msg = msg;
	}
	
	public RequestPath(MessageFile file) {
		this.file = file;
	}

	public List<String> getUsers() {
		return users;
	}

	public void setUsers(List<String> users) {
		this.users = users;
	}

	public MessageText getMsg() {
		return msg;
	}

	public void setMsg(MessageText msg) {
		this.msg = msg;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	

	public MessageFile getFile() {
		return file;
	}

	public void setFile(MessageFile file) {
		this.file = file;
	}

	/**
	 * Responde se o codigo do tipo informado corresponde ao atual
	 * @param tipo
	 * @return
	 */
	public boolean validar(int tipo) {
		return this.tipo == tipo;
	}

}
