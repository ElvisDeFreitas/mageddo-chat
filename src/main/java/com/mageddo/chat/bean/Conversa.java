package com.mageddo.chat.bean;

import java.util.ArrayList;
import java.util.List;

public class Conversa {
	private List<MessageText> mensagens;
	private int lastSaved = 0;

	public Conversa() {
		mensagens = new ArrayList<MessageText>();
	}
	
	public List<MessageText> getMensagens() {
		return mensagens;
	}

	public void setMensagens(List<MessageText> mensagens) {
		this.mensagens = mensagens;
	}

	public int getLastSaved() {
		return lastSaved;
	}

	public void setLastSaved(int lastSaved) {
		this.lastSaved = lastSaved;
	}

}
