package com.mageddo.chat.bean;

import org.apache.commons.lang3.text.WordUtils;



public class UserClient {
	private String nick;
	private String nameView;
	private Object[] row = new Object[1];

	public UserClient() {
		row[0] = this;
	}

	public Object[] getRow() {
		return row;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
		this.nameView = WordUtils.capitalizeFully(nick);
	}


	/**
	 * Método para ser comparado corretamente
	 */
	@Override
	public String toString() {
		return nameView;
	}
	
	@Override
	public boolean equals(Object obj) {
		return nick.equals(obj);
	}

}
