package com.mageddo.chat.bean;

public class MessageText extends Message{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2082007904181781601L;

	public MessageText() {
		super();
	}
	
	public MessageText(String msg) {
		super();
		message = msg;
	}
	
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
