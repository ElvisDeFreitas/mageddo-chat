package com.mageddo.chat.gui;

import java.awt.Component;
import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;

import com.mageddo.chat.events.FileChooseEvent;

public class FileSendChooser {
	Component parent = null;
	FileChooseEvent e;
	File current;
	public FileSendChooser(Component parent, FileChooseEvent e) {
		this.parent = parent;
		this.e = e;
		current = new File(System.getProperty("user.home"));
	}

	public void open(){
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(current);
		int result = fileChooser.showOpenDialog(parent);
		 if (result == JFileChooser.APPROVE_OPTION) {
			 File file = fileChooser.getSelectedFile();
			 e.action(file, FileChooseEvent.OK_FILE);
			 try {
				current = file.getCanonicalFile();
			} catch (IOException e1) {}
         } else {
        	 e.action(null, FileChooseEvent.CANCEL_FILE);
         }

	}
}
