package com.mageddo.chat.gui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.google.gson.JsonSyntaxException;
import com.mageddo.chat.Util;
import com.mageddo.chat.file.ChatPath;
import com.mageddo.chat.history.HistorySaver;
import com.mageddo.chat.history.HistoryUtil;

public class HistoryGUI extends JFrame implements ListSelectionListener{

	private static final long serialVersionUID = -2467456855289452704L;
	private JList list;
	private ChatPath path;
	private JSplitPane splitPane;  
	private JTextArea visualizador;
	private DefaultListModel listModel;
	private HashMap<String, List> texts;
	
	public HistoryGUI(ChatPath path) {
		this.path = path;
		texts = new HashMap();
		configureComponents();
	}
	
	private void configureComponents() {
		configurarLista();
		visualizador.setLineWrap(true);;
		visualizador.setEditable(false);
		setTitle(Util.prop("chat.historyVisualizer"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	private void configurarLista(){
		listModel = new DefaultListModel();
		
		// populando lista de históricos
		File file = new File(path.getChatHistoryPath());
		for(Object o : HistoryUtil.getListaHistorico(file)){
			listModel.addElement(o.toString());
		}

	    //Create the list and put it in a scroll pane.
	    list = new JList(listModel);
	    list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	    list.setSelectedIndex(0);
	    list.addListSelectionListener(this);
	    list.setVisibleRowCount(1000);
		visualizador = new JTextArea();
		splitPane = new JSplitPane(
			JSplitPane.HORIZONTAL_SPLIT,
			new JScrollPane(list), new JScrollPane(visualizador)
		);

		splitPane.setOneTouchExpandable(true);
		splitPane.setDividerLocation(130);
		getContentPane().add(splitPane);

		// carregando conteúdo do primeiro item
		carregarHistorico();
		
		
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		carregarHistorico();
	}
	
	private void carregarHistorico() {
		if(texts.containsKey(list.getSelectedValue())){
			visualizador.setText(TextVisualizer.mountMessage(texts.get(list.getSelectedValue())));
		}else{
			String msg = null;
			if(list.getSelectedValue() != null){
				HistorySaver h = new HistorySaver(path);
				File historico = new File(path.getChatHistoryPath() + "/" + list.getSelectedValue() + "." + Util.HISTORY_EXTENSION );
				try {				
					texts.put(list.getSelectedValue().toString(), h.ler(historico));
					visualizador.setText(TextVisualizer.mountMessage(texts.get(list.getSelectedValue())));
				}catch (FileNotFoundException e) {
					msg = Util.prop("chat.history.notfound");
				} catch (JsonSyntaxException e1) {
					msg = Util.prop("chat.history.corrupted") + ":\n" + historico.getAbsolutePath();
				} catch (IOException e1) {
					msg = Util.prop("chat.history.ioerror");
				}
			}else{
				msg = Util.prop("chat.history.empty");
			}			
			if(msg != null){
				JOptionPane.showMessageDialog(
					this,
					msg,
					Util.prop("chat.history.error"),
					JOptionPane.WARNING_MESSAGE
				);
				
				
			}
		}
		
	}	
	
	

}
