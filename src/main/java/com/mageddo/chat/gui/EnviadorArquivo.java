package com.mageddo.chat.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;

import javax.naming.LimitExceededException;
import javax.swing.JOptionPane;

import org.apache.commons.io.FileUtils;

import com.mageddo.chat.Util;
import com.mageddo.chat.bean.MessageFile;
import com.mageddo.chat.bean.MessageText;
import com.mageddo.chat.bean.RequestPath;
import com.mageddo.chat.bean.UserClient;
import com.mageddo.chat.events.FileChooseEvent;
import com.mageddo.chat.file.FileWritter;
import com.mageddo.chat.run.ChatClient;
import com.mageddo.chat.util.Configuration;

public class EnviadorArquivo {

	private ChatClient chat;
	
	public EnviadorArquivo(ChatClient chat) {
		this.chat = chat;
	}
	
	/**
	 * Executa todo o processo de Escolher o arquivo e o enviar
	 */
	public void enviar(){
		final FileSendChooser chooser = new FileSendChooser(chat, new FileChooseEvent() {
			String msg = null;
			@Override
			public void action(File file, int status) {
				if(FileChooseEvent.CANCEL_FILE == status)
					return ;
		
				try {
					msg = null;
					
					// verificando tamanho máximo
					if(file.length() > new Integer(Util.getConfig().get(Configuration.MAX_FILE_SEND_SIZE)))
						throw new LimitExceededException();

					byte[] bytes = FileUtils.readFileToByteArray(file);
					
					// setando informações do arquivo
					MessageFile msg = new MessageFile();
					msg.setFrom(chat.getMyNick());
					msg.setLength(bytes.length);
					msg.setFileName(file.getName());
					msg.add(chat.getListaUsuariosModelo().getSelectedClient(chat.getListaUsuarios()).getNick());
					RequestPath req = new RequestPath(msg);
					req.setTipo(RequestPath.ARQUIVO);
					
					// setando mensagem
					MessageText msgTxt = new MessageText();
					msgTxt.setFrom(chat.getMyNick());
					msgTxt.setData(Calendar.getInstance());
					msgTxt.getTo().clear();
					msgTxt.setMessage(Util.prop("chat.file.sending") + file.getName());
					req.setMsg(msgTxt);
					
					// enviando mensagem
					chat.mensagemFromClient(req);
					
					// enviando bytes do arquivo
					FileWritter.writeToOut(bytes, chat.getSocket().getOutputStream());
					
				} catch (IOException e) {
					msg = Util.prop("chat.file.send.error");
				} catch (LimitExceededException e) {
					msg = Util.prop("chat.file.sizetoolong") + (new Integer(Util.getConfig().get(Configuration.MAX_FILE_SEND_SIZE)) / 1000) + "KB atingido";
				}
				if(msg != null){
					JOptionPane.showMessageDialog(chat, msg, Util.prop("chat.error.fatalErorr"), JOptionPane.ERROR_MESSAGE);
				}
	
			}
		});
		
		
		chat.getArquivo().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				// deve selecionar um usuário antes
				final UserClient client = chat.getListaUsuariosModelo().getSelectedClient(chat.getListaUsuarios());
				if(chat.getPath() == null){
					return ;
				}else if(client == null){
					chat.mensagem(Util.prop("chat.warn.selectUser"));
					return ;
				}
				
				// abrindo chooser
				chooser.open();
				
			}
		});		
	}

}
