package com.mageddo.chat.gui;

import java.util.List;

import com.mageddo.chat.bean.Conversa;
import com.mageddo.chat.bean.MessageText;
import com.mageddo.chat.bean.RequestPath;
import com.mageddo.chat.bean.UserClient;
import com.mageddo.chat.run.ChatClient;

public class Messenger {
	private ChatClient chat;

	public Messenger(ChatClient chat) {
		this.chat = chat;
	}
	
	/**
	 * Monta o objeto de requsição com base nas Strings passadas e serializa-o
	 * para enviar para o servidor
	 * 
	 * @param key
	 * @param message
	 * @param fromMe
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void novaMensagem(String key, MessageText message, boolean fromMe) {
		
		// Jogando mensagem no buffer do usuário  
		UserClient user = chat.getListaUsuariosModelo().getClient(key);
		if(chat.getMessages().get(user.getNick()) != null){
			((List) chat.getMessages().get(user.getNick()).getMensagens()).add(message);			
		}else{
			Conversa conversa = new Conversa();
			conversa.getMensagens().add(message);
			chat.getMessages().put(user.getNick(), conversa);
		}

		// se for o usuário atual mostra o conteudo
		UserClient userSelecionado = chat.getListaUsuariosModelo().getSelectedClient(chat.getListaUsuarios());
		if (userSelecionado != null && userSelecionado.equals(key)) {
			chat.getTextArea().setText(TextVisualizer.mountMessage((List) chat.getMessages().get(userSelecionado.getNick()).getMensagens()));
			chat.descerScroll();
		}else{
			// destacando o usuário que acabou de enviar uma mensagem
			if(!fromMe){
				chat.getListaUsuariosModelo().notificarNovaMensagem(chat.getListaUsuarios(), key);
			}
		}

	}
	
	public void novaMensagem(String key, MessageText message) {
		novaMensagem(key, message, false);
	}
	
	/**
	 * Manda uma nova mensagem para o servidor serializando o objeto de
	 * requisição
	 * 
	 * @param req
	 */
	public void novaMensagem(RequestPath req) {
		String key = req.getMsg().getFrom();
		novaMensagem(key, req.getMsg());
	}
	
}
