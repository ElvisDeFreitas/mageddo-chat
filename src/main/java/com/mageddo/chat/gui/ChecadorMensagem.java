package com.mageddo.chat.gui;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import com.mageddo.chat.Util;
import com.mageddo.chat.bean.MessageText;
import com.mageddo.chat.bean.RequestPath;
import com.mageddo.chat.bean.User;
import com.mageddo.chat.exceptions.ErroDesconhecidoException;
import com.mageddo.chat.exceptions.PedidoFechamentoException;
import com.mageddo.chat.file.ChatPath;
import com.mageddo.chat.history.HistorySaver;
import com.mageddo.chat.logger.Logger;
import com.mageddo.chat.run.ChatClient;
import com.mageddo.chat.util.Configuration;

public class ChecadorMensagem {
	ChatClient chat;
	public ChecadorMensagem(ChatClient chatClient) {
		this.chat = chatClient;
	}
	public void run() throws ErroDesconhecidoException, UnknownHostException, IOException, PedidoFechamentoException {
		chat.setTitle("Chat " + Util.VERSAO_CLIENT);
		// Make connection and initialize streams
		chat.setSocket(new Socket(Util.getConfig().get(Configuration.SERVER_IP), new Integer(Util.getConfig().get(Configuration.SERVER_PORT))));
		chat.setUser(new User(chat.getSocket().getInputStream(), chat.getSocket().getOutputStream())); 

		// informando a versão do chat (Versões anterioes a 0.0.6 nao fazem isso)
		chat.getUser().writeLine(Util.VERSAO_CLIENT);
		
		while (true) {

			chat.setRequest(chat.getUser().readObject(RequestPath.class));
			
			switch (chat.getRequest().getTipo()) {
			case RequestPath.ARQUIVO:
				chat.recebeArquivo();
				chat.getRequest().getMsg().setMessage(
						Util.prop("chat.file.savedOn") + chat.getPath().getChatFilesPath() +
						"/" + chat.getRequest().getFile().getFileName()
						);
				chat.messenger.novaMensagem(chat.getRequest());
				chat.toFront();
				break;
			case RequestPath.LOGIN:
				chat.getUser().writeLine((chat.setMyNick(chat.getAutoUserName())));;
				break;
			case RequestPath.LOGIN_MANUAL:
				chat.getUser().writeLine((chat.getUserName()) + " - " + Util.getComputerName(" * "));
				break;
			case RequestPath.LOGIN_ACEITO:
				chat.setMyNick(chat.getRequest().getMsg().getMessage());
				Logger.log("login efetuado como " + chat.getMyNick());
				chat.getEditor().setEditable(true);
				chat.setTitle("Chat " + Util.VERSAO_CLIENT + " - " + chat.getMyNick());
				chat.getTextArea().append(Util.prop("chat.connected"));
				chat.getTextArea().append("\n");
				chat.conectado = true;
				chat.setPath(new ChatPath(chat.getMyNick()));
				chat.setHistorySaver(new HistorySaver(chat.getPath()));
				chat.getHistorySaver().createIfNotExistsDefaultFolder(); // criando a estrutura dos diretórios
				break;
			case RequestPath.MESSAGEM:
				chat.messenger.novaMensagem(chat.getRequest());
				chat.descerScroll();
				chat.toFront();
				break;
			case RequestPath.USERS:
				// atualizando lista
				chat.atualizaUsuarios();
				// atualizando titulo da coluna
				chat.changeTheUsersCollumTitle();
				break;
			case RequestPath.DISCONNECT:
				throw new ErroDesconhecidoException(chat.getRequest().getMsg().getMessage());
			case RequestPath.MENSAGEM_PARA_CLIENTE:
				chat.mensagem(chat.getRequest().getMsg().getMessage());
				break;
			
			case RequestPath.NOTIFICA_NOVA_VERSAO:
				boolean aceita = false;
				RequestPath r = new RequestPath(new MessageText());
				
				// confirmando preferência de atualização
				if(chat.confirmar(chat.getRequest().getMsg().getMessage())){
					r.setTipo(RequestPath.ATUALIZACAO_NOVA_VERSAO_ACEITA);
					aceita = true;
				}else{
					r.setTipo(RequestPath.ATUALIZACAO_NOVA_VERSAO_NEGADA);										
				}
				
				chat.getUser().writeObject(r);
				chat.toFront();
				
				if(aceita){
					chat.processarAtualizacao();
				}
				
				break;
			case RequestPath.NOTIFICA_NOVA_VERSAO_REQUERIDA:
				chat.setRequest(new RequestPath(new MessageText()));
				chat.getRequest().setTipo(RequestPath.ATUALIZACAO_NOVA_VERSAO_ACEITA);
				chat.getUser().writeObject(chat.getRequest());
				chat.toFront();
				chat.processarAtualizacao();
			break;
				
			default:
				throw new ErroDesconhecidoException(Util.prop("chat.error.unexpected"));
			}
		}
	}
	
}
