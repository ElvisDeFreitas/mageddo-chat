package com.mageddo.chat.gui;

import java.util.List;

import org.apache.commons.lang3.text.WordUtils;

import com.mageddo.chat.Util;
import com.mageddo.chat.bean.MessageText;

public class TextVisualizer {
	/**
	 * Retorna a string da mensagem montada
	 * 
	 * @param user
	 * @param message
	 * @return
	 */
	public static String mountMessage(List<MessageText> message) {
		if (message != null) {
			StringBuilder str = new StringBuilder();
			for (MessageText msg : message) {
				str.append(WordUtils.capitalizeFully(msg.getFrom()) + ":  " + Util.getData(msg.getData())
						+ "\n  " + msg.getMessage() + "\n\n");
			}
			return str.toString();
		} else {
			return "";
		}
	}

}
