package com.mageddo.chat.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Rectangle;
import java.util.Set;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import com.mageddo.chat.bean.Conversa;
import com.mageddo.chat.bean.UserClient;
import com.mageddo.chat.guicomponent.ChatListaModel;
import com.mageddo.chat.run.ChatClient;

/**
 * Encarregada de destacar graficamente o usuário que acabou de enviar uma mensagem tão bem como <br/>
 * também remover o destaque quando se é clicado no mesmo. 
 * @author m2_user
 *
 */
public class DestacaUsuario {
	private Set<Integer> hitRow = null;
	private ChatClient chat;
	
	public DestacaUsuario(ChatClient chat) {
		this.chat = chat;
	}

	public void checaDestaque(){
		chat.getListaUsuarios().setDefaultRenderer(Object.class, new DefaultTableCellRenderer(){
			private static final long serialVersionUID = -2894805611563799422L;

			@Override
			public Component getTableCellRendererComponent(JTable table,
					Object value, boolean isSelected, boolean hasFocus,
					int row, int column) {
				Component c; 
				c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, 0);
				boolean alguem = false;
			
			    hitRow = getListaNovasMensagens(table);
				if (hitRow != null) {
					for (Integer i : hitRow) {
						if (i.intValue() == row) {
							alguem = true;
						}
					}
				}
				if(alguem)
					c.setBackground(Color.ORANGE);
				else
					c.setBackground(Color.WHITE);
					
				return super.getTableCellRendererComponent(table, value, isSelected, hasFocus,
						row, column);
			}
		});
	}
	public void removeDestaque(){
		Set<Integer> hitRow = getListaNovasMensagens();
		try{
			// Mostrando as mensagens do usuários
			UserClient usuariosSelecionado = chat.getListaUsuariosModelo().getSelectedClient(chat.getListaUsuarios());
			if (usuariosSelecionado != null) {
				Conversa conversa = chat.getMessages().get(usuariosSelecionado.getNick());
				if(conversa != null){
					chat.getTextArea().setText(TextVisualizer.mountMessage(conversa.getMensagens()));
					chat.descerScroll();
				}else{
					chat.getTextArea().setText("");
				}
			}
			
			// Desmarca o usuário clicado
			hitRow.remove(new Integer(chat.getListaUsuarios().getSelectedRow()));
			Rectangle cell = chat.getListaUsuarios().getCellRect(0, chat.getListaUsuarios().getSelectedRow(), false);
			chat.getListaUsuarios().repaint(cell);
			chat.getListaUsuariosModelo().fireTableRowsUpdated(chat.getListaUsuarios().getSelectedRow(), chat.getListaUsuarios().getSelectedRow());
		}catch(Exception ex){};
	}
	
	/**
	 * Retorna a lista com os indexes dos usários que mandaram uma mensagem ainda não lida
	 * @return
	 */
	public Set<Integer> getListaNovasMensagens() {
		return getListaNovasMensagens(chat.getListaUsuarios());
	}
	public Set<Integer> getListaNovasMensagens(JTable table) {
		return (Set<Integer>) table.getClientProperty(ChatListaModel.NOVOS_USERS);
	}
}
