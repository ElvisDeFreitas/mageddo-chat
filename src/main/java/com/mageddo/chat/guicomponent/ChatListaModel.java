package com.mageddo.chat.guicomponent;

import java.awt.Rectangle;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.mageddo.chat.Util;
import com.mageddo.chat.bean.UserClient;

public class ChatListaModel extends DefaultTableModel{
	private static final long serialVersionUID = -1712840018965306900L;
	public static final String NOVOS_USERS = "linhasNovas";
	private Set<Integer> usuariosEmNotificacao = new TreeSet<Integer>();
	private static final String TITULO_COLUNA_USUARIOS = Util.prop("chat.users");
	
	public ChatListaModel(Object[] columnNames, int rowCount){
		super(columnNames, rowCount);
	}
	

	/**
	 * Procura o objeto com o valor indicado e o retorna
	 * @param nick
	 * @return
	 */
	public UserClient getClient(Object nick) {
		for (int i = 0; i < this.getRowCount(); i++) {
			if(this.getValueAt(i, 0).equals(nick)){
				return (UserClient) this.getValueAt(i, 0);
			}
		}
		return null;
	}
	
	/**
	 * Retorna a posição do usuário
	 * @param nick
	 * @return
	 */
	public int getClientPos(Object nick) {
		for (int i = 0; i < this.getRowCount(); i++) {
			if(this.getValueAt(i, 0).equals(nick)){
				return i;
			}
		}
		return -1;
	}
	
	
	@Override
	public String getColumnName(int column) {
		return (getRowCount() + 1) + " " + TITULO_COLUNA_USUARIOS;
	}
	
	/**
	 * Notifica o usuário sbre uma nova mensagem destacando o nome na lista
	 */
	public void notificarNovaMensagem(JTable table, Object nick) {
		int rowClient = getClientPos(nick);
		if(rowClient == -1)
			return ;
		
		usuariosEmNotificacao.add(rowClient);
		table.putClientProperty(NOVOS_USERS, usuariosEmNotificacao);
		Rectangle cell = table.getCellRect(0, rowClient, false);
		table.repaint(cell);
		fireTableRowsUpdated(rowClient, rowClient);

	}
	
	
	
	/**
	 * Retorna o usuário selecionado
	 */
	public UserClient getSelectedClient(JTable table){
		if(table.getSelectedRow() == -1)
			return null;
		return (UserClient) getValueAt(table.getSelectedRow(), 0);
	}
	
	/**
	 * Retorna o cliente
	 * @param index
	 * @return
	 */
	public UserClient getClient(int index){
		return (UserClient) getValueAt(index, 0);
	}
	
}
