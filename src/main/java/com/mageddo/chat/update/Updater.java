package com.mageddo.chat.update;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.zip.ZipFile;

import org.apache.commons.io.FileUtils;

import com.ClientApp;
import com.mageddo.chat.Util;
import com.mageddo.chat.exceptions.InvalidFileException;
import com.mageddo.chat.file.ChatPath;
import com.mageddo.chat.file.FileWritter;
import com.mageddo.chat.file.FileZipper;
import com.mageddo.chat.logger.Logger;
import com.mageddo.chat.run.ChatClient;
import com.mageddo.chat.shortcut.Shortcut;

public class Updater {

	/**
	 * Retorna a última versão salva no servidor <br/>
	 * Retorna -1 se não existirem versões
	 * 
	 * @return Integer
	 */
	@SuppressWarnings({ "unchecked" })
	public static Integer getReleases() {
		File file = new File(Util.getReleasesPath());
		List<String> list = FileWritter.listarArquivos(file, false, true);
		if (list == null || list.size() == 0) {
			return -1;
		}
		Collections.sort(list);
		return new Integer(list.get(list.size() - 1));
	}


	/**
	 * Instala os arquivos baixados da atualização
	 */
	public static Object[] instalarArquivos(File file, File folderInstalation) {
		List<String> mensagens = new ArrayList<String>();
		boolean sucesso = true;

		if (!folderInstalation.exists())
			folderInstalation.mkdirs();

		//
		// limpando antigas versões da pasta de instalação menos a ultima
		//

		// pegando pasta de instalação
		File installDirectory = new File(ChatPath.getSystemInstalledPath());

		// pegando versões instaladas
		String[] directories = installDirectory.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) {
				return new File(current, name).isDirectory();
			}
		});

		// sorteando versões numericamente
		Arrays.sort(directories);

		// removendo todos menos última versão instalada
		for (int i = 0; i < directories.length - 1; i++) {
			try {
				FileUtils.deleteDirectory(new File(installDirectory.getAbsolutePath() + File.separator + directories[i]));
			} catch (IOException e) {
				Logger.log("Não foi possível limpar a versão " + directories[i]);
				Logger.log(e);
			}
		}

		// caso a pasta de instalação seja a mesma tenta deletar os itens antigos
		try {
			FileUtils.cleanDirectory(folderInstalation);
		} catch (IOException e1) {
			e1.printStackTrace();
			mensagens.add("Erro ao limpar a pasta de instalação");
			return mensagens.toArray();
		}
		
		// deszipando
		try {
			FileZipper.unZip(new ZipFile(file), folderInstalation);
		} catch (InvalidFileException e) {
			sucesso = false;
			Logger.log(e);
			Logger.log("O diretório de destino não é uma pasta");
		} catch (IOException e) {
			sucesso = false;
			Logger.log(e);
			Logger.log("Erro ao extrair arquivo");
		}

		// se atualizou com sucesso abre o novo software
		if (sucesso) {
			mensagens.add("Software atualizado com sucesso");

			// criando atalho
			if (System.getProperty("os.name").toLowerCase().indexOf("win") >= 0) {
				try {
					Shortcut.createWindowsShortcut(folderInstalation
							.getAbsolutePath());
				} catch (IOException e) {
					Logger.log("Falha ao criar atalho");
				}

			} else {
				try {
					Shortcut.createLinuxShortcut(folderInstalation
							.getAbsolutePath());
				} catch (IOException e) {
					Logger.log("Falha ao criar atalho");
				}

			}
			ClientApp c = new ClientApp();
			c.setCaminho(folderInstalation.getAbsolutePath());
			c.start();
			try {
				ClientApp.sleep(1000L);
			} catch (InterruptedException e) {
			}
		} else {
			mensagens.add("Ocorreu um erro na atualização");
		}
		return mensagens.toArray();
	}

	/**
	 * Recebe atualização do servidor
	 */
	public static File baixarAtualizacao(ChatClient chat) {

		// baixando informações do arquivo de atualização
		try {
			chat.setRequest(chat.getUser().readRequestPath());
		} catch (Exception e) {
			Logger.log(e);
			chat.erro("Erro na leitura das informações do arquivo de atualização");
		}

		// limpando pasta de downloads
		try {
			FileUtils
					.cleanDirectory(new File(ChatPath.getSystemDownloadPath()));
		} catch (IOException e1) {
			Logger.log("Log ao limpar pasta de downloads");
			Logger.log(e1);
		}

		File file = new File(ChatPath.getSystemDownloadPath() + "/"
				+ chat.getRequest().getFile().getFileName());
		try {
			byte bytes[] = new byte[new Long(chat.getRequest().getFile()
					.length()).intValue()];
			chat.getUser().lerBytes(bytes);
			FileWritter.writeFile(bytes, file);
		} catch (IOException e) {
			Logger.log(e);
			chat.mensagemAtencao("Erro ao receber arquivo");
		}

		return file;

	}

}
