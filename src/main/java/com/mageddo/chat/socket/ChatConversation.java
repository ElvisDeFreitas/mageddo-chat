package com.mageddo.chat.socket;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.mageddo.chat.Util;
import com.mageddo.chat.bean.RequestPath;

public class ChatConversation {
	private InputStream in;
	private OutputStream out;
	private List<Byte> chars;
	private static final Gson serializer = new Gson();

	public ChatConversation(InputStream in, OutputStream out) {
		this.in = in;
		this.out = out;
		chars = new ArrayList();
	}
	
	/**
	 * Lê de linha em linha
	 * Verifica se existe um '\n' se existir le mais um byte e verifica se é um \r se for returna o conteudo 
	 * se não adiciona os caracteres e continua a leitura
	 * @return
	 * @throws IOException
	 */
	public String readLine() throws IOException {
		Byte last = 0x10;
		chars.clear();

		Byte i;
		while ((i = (byte) in.read()) != -1) {
			if(i == last){
				if((i = (byte) in.read()) == 0x13)// \r
					break;
				else{
					chars.add(last);
					chars.add(i);
				}
			}else{
				chars.add((byte) i);
			}
		}
			
		return new String(Util.toByteArray(chars));
	}

	/**
	 * @param str
	 * @throws IOException
	 */
	public void writeObject(Object o) throws IOException {
		writeLine(serializer.toJson(o));
	}
	
	
	/**
	 * Lê um objeto serializado do GSON e enviado pelo @writeObject @see {@link ChatConversation}
	 * @param type
	 * @return 
	 * @return
	 * @throws JsonSyntaxException
	 * @throws IOException
	 */
	public <T>T readObject(Class<T> type) throws JsonSyntaxException, IOException {
		return (T) serializer.fromJson(readLine(), type);

	}
	
	/**
	 * Atalho para leitura de Requests
	 * @return {@link RequestPath}
	 * @throws JsonSyntaxException
	 * @throws IOException
	 */
	public RequestPath readRequestPath() throws JsonSyntaxException, IOException {
		return serializer.fromJson(readLine(), RequestPath.class);
		
	}
	
	public void writeLine(String str) throws IOException{
		out.write((str).getBytes());
		out.write(0x10);
		out.write(0x13);
		out.flush();
	}
	
	/**
	 * Escreve o arquivo para o output stream e faz um flush
	 * @throws IOException 
	 */
	public void writeToOut(byte[] data) throws IOException{
		writeToOut(data, out);
	}
	
	/**
	 * Escreve o arquivo para o output stream e faz um flush
	 * @throws IOException 
	 */
	public void writeToOut(byte[] data, OutputStream out) throws IOException{
		out.write(data, 0, data.length);
		out.flush();
	}
		
	/**
	 * Lê os dados de um inputStream de socket para o array de bytes
	 */
	public byte[] lerBytes(byte[] bytes) throws IOException{
		int bytesLidos;
		int corrente = 0;
		
		do {
			bytesLidos = in.read(bytes, corrente, bytes.length - corrente);
			if (bytesLidos != -1)
				corrente += bytesLidos;
		} while (corrente != bytes.length);
		
		return bytes;
	}
	
	/**
	 * Retorna os bytes de um arquivo
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public byte[] getFileBytes(File file) throws IOException{
		FileInputStream in = new FileInputStream(file);
		byte[] array =  IOUtils.toByteArray(in);
		in.close();
		return array;
	}

	public InputStream getIn() {
		return in;
	}

	public void setIn(InputStream in) {
		this.in = in;
	}

	public OutputStream getOut() {
		return out;
	}

	public void setOut(OutputStream out) {
		this.out = out;
	}

	
	

	
}
