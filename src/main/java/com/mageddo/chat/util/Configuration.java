package com.mageddo.chat.util;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import com.Raiz;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.mageddo.chat.file.FileWritter;

public class Configuration {
	public static final String APP_MEMORY_LIMIT = "limitMemory";
	public static final String SERVER_PORT = "serverPort";
	public static final String SERVER_IP = "serverIp";
	public static final String MAX_FILE_SEND_SIZE = "maxFileSendSize";
	public static final String LANGUAGE = "language";
	private File configuration;

	public Configuration() {
		configuration = new File(
				new File(Raiz.getRaiz("").getPath()).getAbsolutePath()
						+ "/config.conf");
	}

	/**
	 * Lê arquivo de configuração
	 * 
	 * @return
	 * @throws JsonSyntaxException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	public HashMap<String, String> read() throws JsonSyntaxException, IOException {
		return new Gson().fromJson(FileWritter.readTextFile(configuration),
				HashMap.class);
	}

	/**
	 * Escreve arquivo de confiuração padrão
	 * 
	 * @throws IOException
	 */
	public void writeDefault() throws IOException {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put(Configuration.APP_MEMORY_LIMIT, "60m");
		map.put(Configuration.SERVER_PORT, "9003");
		map.put(Configuration.SERVER_IP, "127.0.0.1");
		map.put(Configuration.MAX_FILE_SEND_SIZE, "3000000");
		map.put(Configuration.LANGUAGE, "en");

		FileWritter.writeFile(
			new GsonBuilder()
			.setPrettyPrinting()
			.serializeNulls()
			.create()
			.toJson(map).getBytes(), configuration
		);
	}

}
