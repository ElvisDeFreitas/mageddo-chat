package com.mageddo.chat.run;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.google.gson.JsonSyntaxException;
import com.mageddo.chat.Util;
import com.mageddo.chat.bean.MessageFile;
import com.mageddo.chat.bean.MessageText;
import com.mageddo.chat.bean.RequestPath;
import com.mageddo.chat.bean.User;
import com.mageddo.chat.logger.Logger;
import com.mageddo.chat.socket.ChatConversation;
import com.mageddo.chat.update.Updater;
import com.mageddo.chat.util.Configuration;

public class ChatServer {

	/**
	 * lista com os outputs dos usuários chaveados por nick
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static HashMap<String, User> writers = new HashMap();

	/**
	 * lista dos nomes dos usuários logados
	 */
	private static List<String> names = new ArrayList<String>();

	/**
	 * The main method appplication, Which just listens on a port and Spawns
	 * handler threads.
	 */
	public static void main(String[] args) throws Exception {
		Logger.log("Iniciando o servidor...");
		ServerSocket listener = new ServerSocket(new Integer(Util.getConfig()
				.get(Configuration.SERVER_PORT)));
		Logger.log("Servidor iniciado.\n");
		try {
			while (true) {
				new Handler(listener.accept()).start();
			}
		} finally {
			listener.close();
		}
	}

	/**
	 * The handler thread class. Handlers are spawned from the listening Loop
	 * and are Responsible for the dealing with a single client And broadcasting
	 * its messages.
	 */
	private static class Handler extends Thread {
		private String name;
		private Socket socket;
		private User user;
		private RequestPath request = new RequestPath();
		private RequestPath msg;
		private byte[] bytes;

		public Handler(Socket socket) {
			this.socket = socket;
		}

		public void serialize(Object o) throws IOException {
			serialize(user, o);
		}

		public void serialize(ChatConversation out, Object o)
				throws IOException {
			out.writeObject(o);
		}

		/**
		 * Método de execução da Thread
		 */
		public void run() {
			try {

				user = new User(socket.getInputStream(),
						socket.getOutputStream());
				msg = new RequestPath();

				// checa versão
				verificarVersao();

				// Checando atualizações
				autoUpdate();

				// loga no server
				logar();

				// Chegando aqui o nick foi aceito então é feita a resposta
				// positiva para o client
				boasVindas();

				// solicitando atualização lista de usuarios
				atualizarListaUsuarios();

				// inciando serviço do chat
				chatService();

			} catch (IOException e) {
				Logger.log("DESCONECTADO >> " + name + "\n\t" + e.getMessage());
			} finally {
				removeUsuario();
				fecharConexoes();
			}

		}

		/**
		 * Fecha as conexões do server
		 */
		public void fecharConexoes() {
			try {
				socket.close();
			} catch (IOException e) {
			}
			try {
				user.getIn().close();
			} catch (IOException e) {
			}
			user.close();
		}

		/**
		 * Serviço de chat do Servidor
		 * 
		 * @throws IOException
		 */

		private void chatService() throws IOException {
			// inciando serviço do chat
			while (true) {
				// descompactação do objeto de requisição
				msg = user.readRequestPath();

				// verificando o tipo de requisição e trazendo o serviço
				// adequado
				switch (msg.getTipo()) {
				case RequestPath.KILL_IN_SERVER:
					throw new IOException();

				case RequestPath.MESSAGEM:

					/*
					 * Tendo certeza de que o client NÃO vá forçar falsidade
					 * ideologica
					 */
					msg.getMsg().setFrom(name);
					msg.getMsg().setData(Calendar.getInstance());

					// pegando lista de destinatários e mandando a mensagem
					for (String nick : msg.getMsg().getTo()) {
						serialize(writers.get(nick), msg);
					}
					msg = null;
					break;

				case RequestPath.ARQUIVO:
					/*
					 * Tendo certeza de que o client NÃO vá forçar falsidade
					 * ideologica
					 */
					msg.getFile().setFrom(name);
					msg.getFile().setData(Calendar.getInstance());

					if (msg.getFile().length() < new Integer(Util.getConfig()
							.get(Configuration.MAX_FILE_SEND_SIZE))) {
						bytes = new byte[new Long(msg.getFile().length())
								.intValue()];
						user.lerBytes(bytes);
					} else {
						mensagemParaClient(Util.prop("chat.file.sizetoolong")
								+ (new Integer(Util.getConfig().get(
										Configuration.MAX_FILE_SEND_SIZE)) / 1000)
								+ "KB");
						continue;
					}

					// pegando lista de destinatários e mandando a mensagem
					for (String nick : msg.getFile().getTo()) {

						// mandando solicitação de recebimento de arquivo
						serialize(writers.get(nick), msg);

						// mandando o arquivo
						writers.get(nick).writeToOut(bytes);

					}
					break;

				default:
					// comando desconhecido matando a conexão
					sayBye("");

				}
			}

		}

		/**
		 * Verifica se o usuário suporta o auto update e o faz se positivo
		 */
		private void autoUpdate() {
			// verificando se o client suporta auto atualização
			if (user.getVersion() >= Util
					.getIntegerVersionNumber(Util.CLIENT_AUTO_UPDATE_SINCE_SUPPORT)) {

				// verificando se o usuário está desatualizado
				if (user.getVersion() < Updater.getReleases()) {

					msg = new RequestPath(new MessageText());

					// verificando se a atualização é opcional ou obrigatória
					if (user.getVersion() < Util
							.getIntegerVersionNumber(Util.COMPATIBLE_VERSION)) {
						msg.setTipo(RequestPath.NOTIFICA_NOVA_VERSAO_REQUERIDA);
						msg.getMsg()
								.setMessage(
										"Versão "
												+ Updater.getReleases()
												+ Util.prop("chat.version.new"));
					} else {
						msg.setTipo(RequestPath.NOTIFICA_NOVA_VERSAO);
						msg.getMsg().setMessage(
								String.format(Util.prop("chat.version.new.question"), Updater.getReleases()));
					}

					// mandando solicitacao de update
					try {
						serialize(msg);
					} catch (IOException e) {
						Logger.log(e);
					}

					// lendo a resposta do cliente
					try {
						msg = user.readRequestPath();
					} catch (JsonSyntaxException e) {
						Logger.log(e);
						sayBye(Util.prop("chat.version.incorrect.update.response"));
					} catch (IOException e) {
						Logger.log(e);
						sayBye(Util.prop("chat.client.connection.closed"));
					}

					// atualizando
					if (msg.getTipo() == RequestPath.ATUALIZACAO_NOVA_VERSAO_ACEITA) {
						Integer ultimaVersao = Updater.getReleases();
						File file = new File(Util.getReleasesPath() + "/"
								+ ultimaVersao);
						msg = new RequestPath(new MessageFile());
						msg.setTipo(RequestPath.ARQUIVO);
						msg.getFile().setLength(file.length());
						msg.getFile().setFileName(ultimaVersao + "");
						try {
							// mandando informações do arquivo
							serialize(msg);
							bytes = FileUtils.readFileToByteArray(file);
							// mandando o arquivo
							user.writeToOut(bytes);
						} catch (IOException e) {
							Logger.log(e);
							sayBye(Util.prop("chat.update.send.eror")	+ user.getNick());
						}

					}

				}
			}

		}

		/**
		 * Da mensagem de boas vindas para o cliente
		 * 
		 * @throws IOException
		 */
		private void boasVindas() throws IOException {
			request.setMsg(new MessageText(name));
			request.setTipo(RequestPath.LOGIN_ACEITO);
			serialize(request);
			Logger.log("CONECTADO >> " + name);

		}

		/**
		 * Verifica se o client que está se conectando suporta o server
		 * 
		 * @throws IOException
		 */
		private void verificarVersao() throws IOException {
			user.setVersion(Util.getIntegerVersionNumber(user.readLine()));

			if (user.getVersion() < Util
					.getIntegerVersionNumber(Util.COMPATIBLE_VERSION)) {
				sayBye(Util.prop("chat.version.unsupported"));
			}
		}

		/**
		 * Autenticação do usuário no sistema
		 * 
		 * @throws IOException
		 */
		private void logar() throws IOException {
			boolean loginAutomatico = true;
			// Roda para autenticar o usuário no server
			// só irá continuar depois de receber a resposta positiva
			while (true) {

				// requisitando autenticação
				if (loginAutomatico) {
					request.setMsg(null);
					request.setTipo(RequestPath.LOGIN);
					serialize(request);
				} else {
					request.setMsg(null);
					request.setTipo(RequestPath.LOGIN_MANUAL);
					serialize(request);
				}
				// pegando resposta
				name = user.readLine().toLowerCase();

				// validação de rotina
				if (name == null) {
					return;
				}

				// tentando autenticar
				synchronized (writers) {
					if (!writers.containsKey(name)) {
						writers.put(name, user);
						user.setNick(name);
						names.add(name);
						break;
					} else {
						loginAutomatico = false;
						mensagemParaClient(Util.prop("chat.login.invalid"));
					}
				}
			}

		}

		/**
		 * Desconecta do servidor mandando uma mensagem
		 * 
		 * @param message
		 */
		private void sayBye(String message) {
			msg.setTipo(RequestPath.DISCONNECT);
			msg.setMsg(new MessageText());
			msg.getMsg()
					.setMessage(message.equals("") ? Util.prop("chat.error.unknow")	: message);
			try {
				serialize(user, msg);
			} catch (IOException e) {
				Logger.log("Erro ao tentar enviar mensagem de despedida para o client\n\t"
						+ e.getMessage());
			}
			removeUsuario();
			fecharConexoes();
			interrupt();

		}

		/**
		 * Manda uma mensagem para o cliente
		 * 
		 * @param message
		 */
		private void mensagemParaClient(String message) {
			msg.setTipo(RequestPath.MENSAGEM_PARA_CLIENTE);
			msg.setMsg(new MessageText());
			msg.getMsg().setMessage(message);
			try {
				serialize(user, msg);
			} catch (IOException e) {
				Logger.log("Erro ao tentar enviar uma mensagem qualquer para o client "
						+ user.getNick() + "\n\t" + e.getMessage());
			}
		}

		/**
		 * Cliente será desconectado e a lista atualizada
		 */
		private void removeUsuario() {
			if (name != null) {
				writers.remove(name);
				names.remove(name);
				atualizarListaUsuarios();

			}
		}

		/**
		 * Atualiza lista de usuários do chat em cada cliente
		 */
		private void atualizarListaUsuarios() {
			request.setTipo(RequestPath.USERS);
			request.setUsers(names);

			for (String name : names) {
				try {
					serialize(writers.get(name), request);
				} catch (IOException e) {
					Logger.log("\nErro ao enviar a mensagem para: " + name
							+ "\n" + e.getMessage() + "\n");
				}
			}
		}
	}

}