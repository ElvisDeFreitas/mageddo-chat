package com.mageddo.chat.run;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.util.Calendar;
import java.util.HashMap;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;

import com.Raiz;
import com.google.gson.Gson;
import com.mageddo.chat.Util;
import com.mageddo.chat.bean.Conversa;
import com.mageddo.chat.bean.MessageText;
import com.mageddo.chat.bean.RequestPath;
import com.mageddo.chat.bean.User;
import com.mageddo.chat.bean.UserClient;
import com.mageddo.chat.events.TimerRunnable;
import com.mageddo.chat.exceptions.ErroDesconhecidoException;
import com.mageddo.chat.exceptions.PedidoFechamentoException;
import com.mageddo.chat.file.ChatPath;
import com.mageddo.chat.file.FileWritter;
import com.mageddo.chat.gui.ChecadorMensagem;
import com.mageddo.chat.gui.DestacaUsuario;
import com.mageddo.chat.gui.EnviadorArquivo;
import com.mageddo.chat.gui.HistoryGUI;
import com.mageddo.chat.gui.Messenger;
import com.mageddo.chat.guicomponent.ChatListaModel;
import com.mageddo.chat.history.HistorySaver;
import com.mageddo.chat.history.HistoryUtil;
import com.mageddo.chat.logger.Logger;
import com.mageddo.chat.update.Updater;

public class ChatClient extends JFrame implements WindowListener {

	private static final long serialVersionUID = 8018073780263155449L;
	private User user;
	private JTextArea editor = new JTextArea(2, 40);
	private JTextArea textArea = new JTextArea();
	private ChatListaModel listaUsuariosModelo;
	private JTable listaUsuarios;
	private Gson serializer = new Gson();
	private RequestPath request = new RequestPath();
	private JScrollPane textAreaScroll;
	private JPanel painelTopo = new JPanel(new BorderLayout());
	private Socket socket;
	public boolean conectado = false;
	private int index = 0;
	private String listaUsuariosnomeColuna[] = { ChatListaModel.NOVOS_USERS };
	private JMenuItem historico;
	private JMenuItem sobre;
	private JMenuItem arquivo;
	private JMenuItem arquivosRecebidos;
	private HashMap<String, Conversa> messages;
	private static ChatPath path;
	private HistorySaver historySaver;
	private EnviadorArquivo enviadorArquivos;
	private DestacaUsuario destacaUsuario;
	public Messenger messenger;

	/**
	 * Chamadas inciais
	 */
	public ChatClient() {
		initializeVariables();
		configureLayout();
		actions();
	}

	/**
	 * Inicializa as instâncias e valores das variáveis
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void initializeVariables() {
		messages = new HashMap();
		enviadorArquivos = new EnviadorArquivo(this);
		destacaUsuario = new DestacaUsuario(this);
		messenger = new Messenger(this);
	}

	/**
	 * Configurações da GUI
	 */
	private void configureLayout() {

		textArea.setLineWrap(true);
		getEditor().setLineWrap(true);

		// criando a parte do topo
		textArea.setEditable(false);

		// GUI Layout
		getEditor().setEditable(false);
		setLayout(new BorderLayout());
		add(new JScrollPane(getEditor()), BorderLayout.PAGE_END);
		add(painelTopo, BorderLayout.NORTH);
		textAreaScroll = new JScrollPane(textArea);
		add(textAreaScroll, BorderLayout.CENTER);

		montaMenu();
		enviaArquivoListener();

		setSize(400, 350);
		configurarLista();

	}

	/**
	 * Método que envia arquivos binários
	 */
	private void enviaArquivoListener() {
		enviadorArquivos.enviar();
	}

	/**
	 * Monta menu
	 */
	private void montaMenu() {
		historico = new JMenuItem(Util.prop("chat.history"));
		sobre = new JMenuItem(Util.prop("chat.about"));
		setArquivo(new JMenuItem(Util.prop("chat.sendfile")));
		arquivosRecebidos = new JMenuItem(Util.prop("chat.file.received"));

		JMenuBar menuBar = new JMenuBar();
		menuBar.setLayout(new GridLayout(1, 4));

		setJMenuBar(menuBar);
		menuBar.add(getArquivo());
		menuBar.add(historico);
		menuBar.add(sobre);
	}

	/**
	 * Configura um ícone na janela
	 * @throws MalformedURLException 
	 */
	private void setIcone() throws MalformedURLException {
		File urlIcone = Raiz.getRaiz("files/icon.png");
		ImageIcon icon = new ImageIcon(new URL(urlIcone.getAbsolutePath()));
		setIconImage(icon.getImage());
	}

	/**
	 * Monta a lista de usuários
	 */
	private void configurarLista() {
		listaUsuariosModelo = new ChatListaModel(listaUsuariosnomeColuna, 0);
		listaUsuarios = new JTable(listaUsuariosModelo) {
			private static final long serialVersionUID = -8522549259463461464L;

			@Override
			protected void processMouseEvent(MouseEvent e) {

				if (e.getID() == 502) { // evento de soltagem do mouse
					if (getSelectedRowCount() == 1) { // verificando se foi
														// selecionado apenas 1
						removerDestaqueNoClick();
						editor.requestFocus();
					}
				}
				super.processMouseEvent(e);
			}

			@Override
			public boolean isCellEditable(int arg0, int arg1) {
				return false;
			}

		};

		// painel de scroll
		JScrollPane listaPainel = new JScrollPane(listaUsuarios);
		listaPainel.setPreferredSize(new Dimension(140, 100));

		// listeners
		destacarUsuarioComNovaMensagem();

		// adcionando ao frame
		add(listaPainel, BorderLayout.WEST);
	}

	/**
	 * Atualiza o titulo da coluna de usuários
	 */
	public void changeTheUsersCollumTitle() {
		listaUsuariosModelo.fireTableStructureChanged();
	}

	/**
	 * Listener que ouve as alterações de cor de fundo dos usuários
	 */
	private void destacarUsuarioComNovaMensagem() {
		destacaUsuario.checaDestaque();

	}

	/**
	 * Listerner que tira o destaque feito pelo método @destacarUsuarioComNovaMensagem
	 */
	private void removerDestaqueNoClick() {
		destacaUsuario.removeDestaque();
	}

	/**
	 * Adciona as actions as listas
	 */
	private void actions() {
		creditos();
		eventoEnviarMensagem();
	}

	/**
	 * Intercepta a digitação do usuário e verifica se a mensagem deve ou não
	 * ser enviada
	 */
	private void eventoEnviarMensagem() {
		// Add Listeners
		getEditor().addKeyListener(new KeyListener() {
			RequestPath req;
			MessageText msg;
			boolean apagar = false;

			public void keyReleased(KeyEvent e) {
				if (apagar) {
					getEditor().setText("");
					apagar = false;
				}
			}

			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() != KeyEvent.VK_ENTER) {
					return;
				}

				if (e.isShiftDown()) {
					getEditor().append("\n");
					return;
				}

				if (!conectado) {
					mensagemAtencao(Util.prop("chat.warn.server.disconneted"));
					return;
				}

				req = new RequestPath();
				msg = new MessageText();

				msg.setFrom(getMyNick());
				msg.setData(Calendar.getInstance());
				msg.getTo().clear();
				msg.setMessage(getEditor().getText());

				req.setTipo(RequestPath.MESSAGEM);
				req.setMsg(msg);

				mensagemFromClient(req);

				// limpando o campo de edição
				getEditor().setText("");
				apagar = true;
			}

			public void keyTyped(KeyEvent e) {

			}

		});

	}

	/**
	 * Manda mensagens do client para os usuários selecionados
	 */
	public void mensagemFromClient(RequestPath req) {

		// mandando mensagem para todos os usuários selecionados
		if (listaUsuarios.getSelectedRows().length > 0) {
			for (int index : listaUsuarios.getSelectedRows()) {
				req.getMsg()
						.add(listaUsuariosModelo.getClient(index).getNick());
			}
		} else {
			mensagem(Util.prop("chat.warn.selectUser"));
			return;
		}

		// logando a mensagem do próprio usuário para todos os
		// destinatários
		for (String to : req.getMsg().getTo()) {
			messenger.novaMensagem(to, req.getMsg(), true);
		}

		// escrevendo mensagem para o server
		serialize(req);
	}

	/**
	 * Escreve uma mensagem
	 * 
	 * @param o
	 */
	public void serialize(Object o) {
		try {
			getUser().writeObject(o);
		} catch (IOException e) {
			Logger.log("Erro ao tentar enviar mensagem");
			Logger.log(e);
		}
	}

	/**
	 * Desce o scroll da área de visualização do chat para a ultima linha
	 */
	public void descerScroll() {
		textArea.setCaretPosition(textArea.getDocument().getLength());
	}

	/**
	 * Exibição dos creditos
	 */
	private void creditos() {
		final JFrame frame = this;
		sobre.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String aboutMessage = Util.loadAboutMessage();
				JOptionPane
						.showMessageDialog(
								frame,
								aboutMessage,
								Util.prop("chat.about.title"),
								JOptionPane.INFORMATION_MESSAGE);
			}
		});

		historico.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				if (getPath() == null)
					return;

				File file = new File(path.getChatHistoryPath());
				if (HistoryUtil.getListaHistorico(file).size() <= 0) {
					JOptionPane.showMessageDialog(ChatClient.this,
							Util.prop("chat.warn.nohistory"),
							Util.prop("chat.warn.nohistory.title"),
							JOptionPane.WARNING_MESSAGE);

					return;
				}

				JFrame frame = new HistoryGUI(getPath());
				frame.setSize(400, 300);
				frame.setVisible(true);
				frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
			}
		});

		arquivosRecebidos.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Desktop.getDesktop().open(
							new File(getPath().getChatFilesPath()));
				} catch (IOException e1) {
					JOptionPane.showMessageDialog(ChatClient.this,
							Util.prop("chat.version.unsuportedoperation"),
							Util.prop("chat.warn"), JOptionPane.ERROR_MESSAGE);
				}

			}
		});

	}

	/**
	 * Prompt for and return the address of the server.
	 */
	@Deprecated
	private String getServerAddress() {
		return JOptionPane.showInputDialog(this,
				Util.prop("chat.input.serverip"), Util.prop("chat.warn.welcome"),
				JOptionPane.QUESTION_MESSAGE);
	}

	/**
	 * Pergunta o nome do usuário a utilizar Se o usuário apertar
	 * "Cancelar lanc uma exceção que fechará a aplição"
	 * 
	 * @throws PedidoFechamentoException
	 */
	public String getUserName() throws PedidoFechamentoException {
		String res = JOptionPane.showInputDialog(this, Util.prop("chat.input.choosenick"),
				Util.prop("chat.warn.choosenick"), JOptionPane.PLAIN_MESSAGE);

		if (res != null)
			return res;
		else
			throw new PedidoFechamentoException(Util.prop("chat.loginend"));
	}

	/**
	 * Connects to the server then enters the processing loop.
	 * 
	 * @throws ErroDesconhecidoException
	 * @throws PedidoFechamentoException
	 */
	private void run() throws IOException, ErroDesconhecidoException,
			PedidoFechamentoException {
		new ChecadorMensagem(ChatClient.this).run();
	}

	/**
	 * Recebe o arquivo do servidor e grava na pasta
	 */
	public void recebeArquivo() {
		try {
			byte bytes[] = new byte[new Long(getRequest().getFile().length())
					.intValue()];
			user.lerBytes(bytes);
			FileWritter.writeFile(bytes, new File(getPath().getChatFilesPath()
					+ "/" + getRequest().getFile().getFileName()));
		} catch (IOException e) {
			JOptionPane.showMessageDialog(ChatClient.this,
					Util.prop("chat.error.receiceFile"));
		}
	}


	/**
	 * Faz processo de atualização
	 */
	public void processarAtualizacao() {

		// criando diretorios se nao existir
		File fd = new File(ChatPath.getSystemDownloadPath());
		if (!fd.exists())
			fd.mkdirs();

		File file = Updater.baixarAtualizacao(this);
		textArea.append(Util.prop("chat.warn.updateDownloaded"));
		Object[] mensagens = Updater.instalarArquivos(file,
				new File(ChatPath.getSystemInstalledPath() + "/"
						+ getRequest().getFile().getFileName()));
		for (int i = 0; i < mensagens.length; i++) {
			mensagem(mensagens[i].toString());
		}
		System.exit(0);
		toFront();

	}

	/**
	 * Tenta fechar as conexões
	 */
	public void closeAll() {
		try {
			getSocket().close();
		} catch (IOException e1) {
		}

		try {
			getUser().getIn().close();
			getUser().getOut().close();
		} catch (IOException e) {
		}

	}


	/**
	 * Atualiza lista de usuários
	 */
	public void atualizaUsuarios() {
		// adicionando novos usuários
		for (Object nick : getRequest().getUsers()) {
			if (!nick.equals(getMyNick())
					&& listaUsuariosModelo.getClient(nick) == null) {
				UserClient client = new UserClient();
				client.setNick(nick.toString());
				listaUsuariosModelo.addRow(client.getRow());

			}
		}

		StringBuilder log = new StringBuilder();
		UserClient currentUser;
		// removendo os antigos
		for (int i = 0; i < listaUsuariosModelo.getRowCount(); i++) {
			currentUser = listaUsuariosModelo.getClient(i);
			if (!getRequest().getUsers().contains(currentUser)) {
				listaUsuariosModelo.removeRow(i);

				// verifica se existem conversas a salvar
				Conversa c = messages.get(currentUser.getNick());
				if (c == null || c.getMensagens().size() <= 0)
					continue;
				// salvando o histórico
				getHistorySaver().save(c, currentUser.getNick(), log);
				messages.remove(currentUser.getNick());
				if (destacaUsuario.getListaNovasMensagens() != null) {
					destacaUsuario.getListaNovasMensagens().remove(
							new Integer(i));
				}
			}
		}

		// possíveis erros
		if (log.length() > 1) {
			JOptionPane.showMessageDialog(ChatClient.this, log.toString(),
					Util.prop("chat.warn.saveLogStatus"), JOptionPane.WARNING_MESSAGE);
		}

	}

	/**
	 * Roda a aplicação cliente
	 */
	public static void main(String[] args) throws Exception {
		boolean rodar = true;
		ChatClient client = new ChatClient();
		client.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		client.addWindowListener(client); // adicionando listener para eventos
											// da janela
		client.setVisible(true);
		client.textArea.append(Util.prop("chat.warn.serverConnecting"));
		client.textArea.append("\n");

		while (rodar) {
			try {
				client.run();
			} catch (ErroDesconhecidoException e) {
				JOptionPane.showMessageDialog(client, e.getMessage(),
						Util.prop("chat.error.fatalErorr"), JOptionPane.ERROR_MESSAGE);
				rodar = false;
			} catch (PedidoFechamentoException e) {
				client.erro(e.getMessage());
				rodar = false;
			} catch (Exception e) {
				Thread.sleep(5000);
				client.conectado = false;
				client.textArea.append(Util.prop("chat.error.connetion.lost"));
				client.textArea.append("\n");
				Logger.log(e);
			}
		}
		client.sair();
	}

	/**
	 * Mostra mensagem de erro e encerra a aplicação
	 * 
	 * @param message
	 */
	public void erro(String message) {
		JOptionPane.showMessageDialog(this, message, Util.prop("chat.warn.closing"),
				JOptionPane.ERROR_MESSAGE);
		sair();
	}

	/**
	 * Desconecta o usuário e depois mata a aplicação(non-Javadoc)
	 * 
	 * @see java.awt.event.WindowListener#windowClosing(java.awt.event.WindowEvent)
	 */
	public void windowClosing(WindowEvent e) {
		sair(true);
	}

	/**
	 * Retorna o nome do usuário tratado
	 */
	public String getAutoUserName() {
		return Util.getComputerName("User " + (++index)).replaceAll("WRK7[-_]",
				"");
	}

	private void sair() {
		sair(false);
	}

	/**
	 * Mata a sessão e fecha o programa
	 */
	private void sair(boolean confirm) {

		if (confirm) {
			if (!confirmar(Util.prop("chat.input.exitQuestion"))) {
				return;
			}
		}

		// salvando histórico
		if (conectado) {
			String log = getHistorySaver().save(messages).toString();
			if (!log.equals(""))
				JOptionPane.showMessageDialog(this, log);

		}

		try {
			getRequest().setTipo(RequestPath.KILL_IN_SERVER);
			serialize(getRequest());
		} catch (Exception e) {
		}
		this.dispose();
		System.exit(0);
	}

	public void windowOpened(WindowEvent e) {
	}

	public void windowClosed(WindowEvent e) {
	}

	public void windowIconified(WindowEvent e) {
	}

	public void windowDeiconified(WindowEvent e) {
	}

	public void windowActivated(WindowEvent e) {
	}

	public void windowDeactivated(WindowEvent e) {
	}

	public String getMyNick() {
		return getUser().getNick();
	}

	public ChatListaModel getListaUsuariosModelo() {
		return listaUsuariosModelo;
	}

	public JTable getListaUsuarios() {
		return listaUsuarios;
	}

	public JMenuItem getArquivo() {
		return arquivo;
	}

	public void setArquivo(JMenuItem arquivo) {
		this.arquivo = arquivo;
	}

	public JTextArea getTextArea() {
		return textArea;
	}

	public HashMap<String, Conversa> getMessages() {
		return messages;
	}

	public Socket getSocket() {
		return socket;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	public Gson getSerializer() {
		return serializer;
	}

	public RequestPath getRequest() {
		return request;
	}

	public void setRequest(RequestPath request) {
		this.request = request;
	}

	public static ChatPath getPath() {
		return path;
	}

	public String setMyNick(String myNick) {
		this.getUser().setNick(myNick);
		return myNick;
	}

	public JTextArea getEditor() {
		return editor;
	}

	public void setEditor(JTextArea editor) {
		this.editor = editor;
	}

	public void setPath(ChatPath path) {
		ChatClient.path = path;
	}

	public HistorySaver getHistorySaver() {
		return historySaver;
	}

	public void setHistorySaver(HistorySaver historySaver) {
		this.historySaver = historySaver;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * Mostra uma mensagem
	 * 
	 * @param message
	 */
	public void mensagem(String message) {
		JOptionPane.showMessageDialog(this, message, Util.prop("chat.warn.serverMessage"),
				JOptionPane.INFORMATION_MESSAGE);
	}

	/**
	 * Mensagem de aviso
	 */
	public void mensagemAtencao(String str) {
		JOptionPane.showMessageDialog(this, str, Util.prop("chat.warn"),
				JOptionPane.WARNING_MESSAGE);
	}

	/**
	 * Abre modal com Mensagem passada e retorna true para verdade ou false para
	 * cancelamento
	 * 
	 * @param message
	 *            Mensagem a ser notificada ao cliente
	 * @return boolean
	 */
	public boolean confirmar(String message) {
		Object[] options = { Util.prop("chat.yes"), Util.prop("chat.no") };
		return JOptionPane.showOptionDialog(this, message,
				Util.prop("chat.exit.confirmation"), JOptionPane.PLAIN_MESSAGE,
				JOptionPane.QUESTION_MESSAGE, null, options, options[0]) == 0 ? true
				: false;
	}
}