package com.mageddo.chat.shortcut;

import java.io.File;
import java.io.IOException;

import com.mageddo.chat.file.FileWritter;

public class Shortcut {

	/**
	 * Cria um atalho no Windows
	 * 
	 * @param path
	 * @throws IOException
	 */
	public static void createWindowsShortcut(String path) throws IOException {
		File shortcut = new File(System.getProperty("user.home"),
				"Desktop/Chat.bat");
		shortcut.setExecutable(true);
		String data = "start " + path + "/run.jar";
		FileWritter.writeFile(data.getBytes(), shortcut);

	}

	/**
	 * Cria um atalho no Linux
	 * 
	 * @param path
	 * @throws IOException
	 */
	public static void createLinuxShortcut(String path) throws IOException {
		File shortcut = new File(System.getProperty("user.home"),
				"Desktop/Chat.desktop");
		shortcut.setExecutable(true);
		String data = "";
		data += "[Desktop Entry]\n" + "Version=1.0\n" + "Name=Chat\n"
				+ "Exec=sh -c \"java -cp " + path
				+ "/run.jar com.ClientApp\"\n" + "Terminal=false\n"
				+ "Type=Application";
		FileWritter.writeFile(data.getBytes(), shortcut);

	}

	
}
