package com.mageddo.chat.exceptions;

public class PedidoFechamentoException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4749446342044685399L;

	public PedidoFechamentoException(String string) {
		super(string);
	}
 
}
