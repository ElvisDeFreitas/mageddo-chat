package com.mageddo.chat.exceptions;

public class ErroDesconhecidoException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4749446342044685399L;

	public ErroDesconhecidoException(String string) {
		super(string);
	}
 
}
