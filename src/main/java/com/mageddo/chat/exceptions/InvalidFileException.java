package com.mageddo.chat.exceptions;

public class InvalidFileException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 89539346235510701L;

	public InvalidFileException() {
		super();
	}
	
	public InvalidFileException(String erro) {
		super(erro);
	}
}
