package com.mageddo.chat.events;

public abstract class TimerRunnable implements Runnable, Comparable<Object> {

	private long time;
	private static long originalTime;


	/**
	 * The time interval in millis
	 * 
	 * @param time
	 */
	public TimerRunnable(long time) {
		this.time = time;
		originalTime = time;
	}

	/**
	 * Get the interval in millis
	 * 
	 * @return
	 */
	public long getTime() {
		return time;
	}

	/**
	 * Set de interval in millis
	 * 
	 * @param time
	 */
	public void setTime(long time) {
		this.time = time;
	}

	public static long getOriginalTime() {
		return originalTime;
	}
	
	@Override
	public int compareTo(Object o) {
		if(this.equals(o))
			return 0;
		else
			return -1;
	}
}
