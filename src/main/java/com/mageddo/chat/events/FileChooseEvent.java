package com.mageddo.chat.events;

import java.io.File;

public abstract class FileChooseEvent {
	public static final int OK_FILE = 1;
	public static final int CANCEL_FILE = 2;
	
	public abstract void action(File file, int status);
	
}
