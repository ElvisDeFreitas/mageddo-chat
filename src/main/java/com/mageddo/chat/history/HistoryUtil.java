package com.mageddo.chat.history;

import java.io.File;
import java.util.List;

import com.mageddo.chat.Util;
import com.mageddo.chat.file.FileWritter;

public class HistoryUtil {

	/**
	 * Retorna lista  de históricos sem a extensão
	 * @param file
	 * @return
	 */
	public static List getListaHistorico(File file){
		List list = FileWritter.listarArquivos(file, false, true);
		for(int i = 0; i < list.size(); i++){
			String o = list.get(i).toString(); 
			
			// se o nome do arquivo for maior que o nome da extensão mais o ponto
			if(Util.HISTORY_EXTENSION.length() < o.length() - 1){
				// se o arquivo terminar com a extensão
				if(o.indexOf(Util.HISTORY_EXTENSION) == o.length() - Util.HISTORY_EXTENSION.length()){
					list.set(i, o.substring(0, o.indexOf(Util.HISTORY_EXTENSION) - 1));
				}else{
					list.remove(i);
					i -= 1;
				}
			}else{
				list.remove(i);
				i -= 1;
			}
		}
		return list;
	}
}