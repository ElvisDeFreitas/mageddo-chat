package com.mageddo.chat.history;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.mageddo.chat.bean.Conversa;
import com.mageddo.chat.bean.MessageText;
import com.mageddo.chat.file.ChatPath;
import com.mageddo.chat.file.FileWritter;

public class HistorySaver {
	private Gson serializer;
	private ChatPath pathBase;
	public HistorySaver(ChatPath pathBase) {
		this.pathBase = pathBase;
		serializer = new Gson();
	}
	
	/**
	 * Salva o histórico de conversa dos usuários 
	 * @param table
	 * @return String - log de erros ocorridos
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public StringBuilder save(HashMap messages){
		StringBuilder erros = new StringBuilder();
		erros.delete(0, erros.length());
		Iterator<String> it = messages.keySet().iterator();
		
		while(it.hasNext()){
			
			// recuperando lista da HashMap
			String key = it.next();
			Conversa con = (Conversa) messages.get(key);
			
			save(con, key, erros);
			
		}
		
		return erros;
	}
	
	public StringBuilder save(Conversa conversa, String key){
		return save(conversa, key, new StringBuilder());
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public StringBuilder save(Conversa conversa, String key, StringBuilder log){
		List mensagens = new ArrayList();
		// Montando o arquivo de histórico
		File arquivo = new File(pathBase.getChatHistoryPath() + "/" + key + ".history");
		
		// Verificando se existe algum histórico a adicionar
		try{
			if(arquivo.exists()){
				mensagens = ler(arquivo);
			}
		}catch(IOException e){
			log.append("Erro ao ler o histórico antigo de: " + key + "\n");
		}
		
		// juntando conversas
		if(mensagens != null){
			mensagens.addAll(conversa.getMensagens().subList(conversa.getLastSaved(), conversa.getMensagens().size()));
			conversa.setLastSaved(conversa.getMensagens().size());
		}
		
		// Gravando o novo histórico 
		try {
			FileWritter.writeTextFile(
				arquivo,
				serializer.toJson(mensagens)
			);
			
		} catch (IOException e) {
			log.append("Erro ao salvar o histórico de: " + key + "\n");
		}
		mensagens = null;
		return log;
	}
	
	/**
	 * Cria o diretório padrão se não existir a pasta
	 */
	public void createIfNotExistsDefaultFolder(){
		
		// pasta de diretórios
		File history = new File(pathBase.getChatHistoryPath());
		File receivedFiles = new File(pathBase.getChatFilesPath());
		if(!receivedFiles.exists()){
			receivedFiles.mkdirs();
		}
		if(!history.exists()){
			history.mkdirs();
		}
	}
	
	/**
	 * Lê o arquivo de histórico e o deserializa
	 * @param file
	 * @return
	 * @throws JsonSyntaxException
	 * @throws IOException
	 */
	public List<MessageText> ler(File file) throws JsonSyntaxException, IOException{
		Type t = new TypeToken<List<MessageText>>() {}.getType();
		return serializer.fromJson(FileWritter.readTextFile(file), t);
	}

}
