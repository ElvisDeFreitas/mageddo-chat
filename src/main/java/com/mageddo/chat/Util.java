package com.mageddo.chat;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.slf4j.LoggerFactory;

import com.Raiz;
import com.mageddo.chat.logger.Logger;
import com.mageddo.chat.util.Configuration;

public class Util {
	public static final String VERSAO_CLIENT = "0.2.6";
	public static final String COMPATIBLE_VERSION = "0.0.8";
	public static final String HISTORY_EXTENSION = "history";
	public static final int TEMPO_AUTO_SAVE = 300000;
	public static final String CLIENT_AUTO_UPDATE_SINCE_SUPPORT = "0.2.0";
	private static HashMap<String, String> confg = null;
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(Util.class);
	protected static final Properties lang = loadLang();

	/**
	 * Tenta pegar o nome do computador atual caso negativo retorna o que foi
	 * passado
	 * 
	 * @param other
	 * @return String
	 */
	public static String getComputerName(String other) {
		try {
			InetAddress addr;
			addr = InetAddress.getLocalHost();
			other = addr.getHostName();
		} catch (UnknownHostException ex) {
		}

		return other;
	}

	protected static Properties loadLang() {
		Properties languageProp = new Properties();
		InputStream fileLanguage;
		try {
			fileLanguage = new FileInputStream(new File(Raiz.getRaiz(Util.getConfig().get(Configuration.LANGUAGE) + "-language.properties").toURI()));
			languageProp.load(fileLanguage);
		} catch (FileNotFoundException e) {
			Logger.log("the file language not exists");
		}catch (IOException e) {
			Logger.log("the file language cannot be loaded");
		}
		return languageProp;
	}
	
	/**
	 * Return the legend key value
	 * @param key
	 * @return
	 */
	public static String prop(String key){
		String message = null;
		try{
			message = lang.getProperty(key);
			if(message == null){
				throw new NullPointerException();
			}
			message = loadVariables(message);
		}catch(Exception e){
			logger.error("{} property not found ", key, e);
		}
		return message;
	}

	protected static String loadVariables(String message) {
		message = message.replaceAll("\\$chat.version", Util.VERSAO_CLIENT);
		return message;
	}

	/**
	 * Retorna uma String da data no formato h:m:s d/m/aaaa
	 * 
	 * @return String
	 */
	public static String getData() {
		return getData(Calendar.getInstance());
	}

	public static String getData(Calendar c) {
		String hora = c.get(Calendar.HOUR_OF_DAY) + ":"
				+ c.get(Calendar.MINUTE) + ":" + c.get(Calendar.SECOND);
		String data = c.get(Calendar.DAY_OF_MONTH) + "/"
				+ (c.get(Calendar.MONTH) + 1) + "/" + c.get(Calendar.YEAR);
		return data + "  " + hora;
	}

	public static byte[] toByteArray(List<Byte> array) {
		byte[] b = new byte[array.size()];

		for (int i = 0; i < b.length; i++) {
			b[i] = array.get(i);
		}
		return b;
	}

	/**
	 * Retorna as configurações em arquivo do chat
	 * 
	 * @return
	 */
	public static synchronized HashMap<String, String> getConfig() {

		if (confg == null) {

			File configFile = new File("tmp.tmp");
			Configuration conf = new Configuration();
			try {
				configFile = new File(
						new File(Raiz.getRaiz("").getPath()).getAbsolutePath()
								+ "/config.conf");

				if (!configFile.exists()) {
					configFile.createNewFile();
					conf.writeDefault();
				}

				confg = conf.read();
			} catch (IOException e) {
				logger.error("Erro ao ler arquivo de configuração {}", configFile.getAbsolutePath(), e);
			}
		}
		return confg;

	}

	/**
	 * Retorna o caminho da pasta de novas versões do chat
	 * 
	 * @return String
	 */
	public static String getReleasesPath() {
		File file = Raiz.getRaiz("releases");
		if (!file.exists())
			file.mkdirs();

		return file.getPath();

	}

	/**
	 * Recebe a versão separada por pontos e retona o Integer do valor
	 * 
	 * @param versao
	 * @return Integer
	 */
	public static Integer getIntegerVersionNumber(String versao) {
		return new Integer(versao.replaceAll("\\.", ""));
	}

	public static String loadAboutMessage() {
		String aboutText;
		try {
			aboutText = FileUtils.readFileToString(new File(Raiz.getRaiz(Util.getConfig().get(Configuration.LANGUAGE) + "-about.license").toURI()));
			aboutText = loadVariables(aboutText);
		} catch (Exception e) {
			Logger.log(e);
			aboutText = "Fail to load license conditions";
		}
		return aboutText;
	}

}
