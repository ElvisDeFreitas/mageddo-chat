package com.mageddo.chat.file;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import com.mageddo.chat.exceptions.InvalidFileException;

public class FileZipper {

	/**
	 * Deszipa um arquivo para a pasta de destino 
	 * @param file
	 * @param outFolder
	 * @throws IOException
	 * @throws InvalidFileException
	 */
	public static void unZip(ZipFile file, File outFolder)
			throws IOException, InvalidFileException {

		Enumeration<?> enu = file.entries();
		File parent;
		InputStream is;
		FileOutputStream fos;
		ZipEntry zipEntry;
		File fileIn;
		int length;
		byte[] bytes = new byte[1024];

		if (!outFolder.isDirectory()) {
			throw new InvalidFileException(
					"O diretório de destino não é uma pasta");
		}

		while (enu.hasMoreElements()) {
			zipEntry = (ZipEntry) enu.nextElement();

			fileIn = new File(outFolder.getAbsolutePath() + File.separator
					+ zipEntry.getName());
			if (zipEntry.getName().endsWith("/")) {
				fileIn.mkdirs();
				continue;
			}

			parent = fileIn.getParentFile();
			if (parent != null) {
				parent.mkdirs();
			}

			is = file.getInputStream(zipEntry);
			fos = new FileOutputStream(fileIn);
			while ((length = is.read(bytes)) >= 0) {
				fos.write(bytes, 0, length);
			}
			is.close();
			fos.close();
		}
		file.close();

	}
}
