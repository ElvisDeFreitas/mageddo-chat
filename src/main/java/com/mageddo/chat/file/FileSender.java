package com.mageddo.chat.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

import org.apache.commons.io.IOUtils;

public class FileSender {
	
	/**
	 * Envia o arquivo para o outputStream de destino
	 * @param data
	 * @param out
	 * @throws IOException 
	 */
	public static void sendFile(byte[] data, OutputStream out) throws IOException{
		PrintWriter printer = new PrintWriter(out);
		for(byte byteChar : data){
			printer.print(byteChar);
		}
		printer.close();
		out.flush();
	}
	
	

	
	/**
	 * Retorna os bytes de um arquivo
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public byte[] getFileBytes(File file) throws IOException{
		FileInputStream in = new FileInputStream(file);
		byte[] array =  IOUtils.toByteArray(in);
		in.close();
		return array;
	}
	
	/**
	 * Envia o arquivo para o outputStream de destino
	 * @param data
	 * @param out
	 * @throws IOException 
	 */
	public void sendFile(InputStream in, OutputStream out) throws IOException{
		PrintWriter printer = new PrintWriter(out);
		int bt;
		while((bt = in.read()) != -1){
			printer.print(bt);			
		}
		out.flush();
	}
}
