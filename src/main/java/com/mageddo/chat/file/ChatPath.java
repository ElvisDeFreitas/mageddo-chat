package com.mageddo.chat.file;

public class ChatPath {
	private String userNick;
	
	public ChatPath(String userNick) {
		this.userNick = userNick;
	}
	
	/**
	 * Diretório padrão do usuário
	 * @return String
	 */
	public static String getUserHome(){
		return System.getProperty("user.home");
	}
	
	/**
	 * Diretório dos dados do usuário dentro do chat
	 * @return String
	 */
	public String getChatPath(){
		return getUserHome() + "/chat/" + userNick;		
	}
	
	/**
	 * Diretório dos dados de histórico do usuário
	 * @return String
	 */
	public String getChatHistoryPath(){
		return getChatPath() + "/history";
	}
	
	/**
	 * Diretório dos arquivos recebidos
	 * @return String
	 */
	public String getChatFilesPath(){
		return getChatPath() + "/Received Files";
	}
	
	/**
	 * Retorna o path de instalações da versão de client
	 */
	public static String getSystemPath(){
		return getUserHome() + "/chat/system";
	}
	
	/**
	 * Retorna o path da pasta de download de atualizações da versão de client
	 */
	public static String getSystemDownloadPath(){
		return getSystemPath() + "/download";
	}
	
	/**
	 * Retorna o path da pasta de download de atualizações da versão de client
	 */
	public static String getSystemInstalledPath(){
		return getSystemPath() + "/install";
	}

	public String getNick(){
		return userNick;
	}

}
