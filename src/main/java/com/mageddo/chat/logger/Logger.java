package com.mageddo.chat.logger;

import org.slf4j.LoggerFactory;

public class Logger {
	public static final org.slf4j.Logger logger = LoggerFactory.getLogger(Logger.class);

	/**
	 * Salva String no log para futuro salvamento
	 * @param str
	 */
	public static synchronized void log(String str) {
		logger.info(str);;
	}
	
	public static synchronized void appendToLog(String str){
		logger.info(str);
	}
	
	public static synchronized void appendLine(String str){
		logger.info(str);
	}

	/**
	 * Recebe uma execeção para logar a saida
	 * 
	 * @param e
	 */
	public static void log(Exception e) {
		logger.error(e.getMessage(), e);
	}	
	
}
