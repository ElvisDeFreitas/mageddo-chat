package com;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import com.mageddo.chat.Util;
import com.mageddo.chat.util.Configuration;

public class ClientApp extends Thread {
	private static String caminho;

	public static void main(String[] args) {

		if (args.length == 1) {
			caminho = args[0];
		}

		Thread t = new ClientApp();
		t.start();
		try {
			t.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		t.interrupted();
		System.exit(0);
	}

	public void setCaminho(String caminho) {
		ClientApp.caminho = caminho;
	}

	@Override
	public void run() {

		if (caminho == null) {
			caminho = Raiz.getRaiz("").getPath();
		}
		File dir = new File(caminho);
		HashMap<String, String> map = Util.getConfig();
		System.out.println("Running the application");

		try {
			Process p = new ProcessBuilder("java", "-Xmx" + map.get(Configuration.APP_MEMORY_LIMIT),
					"com.mageddo.chat.run.ChatClient").directory(dir).start();

			System.out.println("Resultados:");
			read(p.getInputStream());

			System.out.println("\n");

			System.out.println("Erros:");
			read(p.getErrorStream());

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void read(InputStream in) throws IOException {
		byte i;
		StringBuilder str = new StringBuilder();
		while ((i = (byte) in.read()) != -1) {
			str.append((char) i);
		}

		System.out.println(str.toString());
	}
}
