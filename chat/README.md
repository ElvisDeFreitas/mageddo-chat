# installation
- extract `chat-x.x.x.zip`
- run `run-server.bat` or `run-server.sh`
- the server is working
- run `run.bat` or `run.sh`
- the client is working
- open other client to can test

# installation of clients in others machines
- extract  `chat-x.x.x.zip`
- configure server ip on `conf/config.conf`
- run `run.bat` or `run.sh`

please send disclaimers/suggestions to edigitalb@gmail.com or create it on [our repository](https://bitbucket.org/ElvisdeFreitas/mageddo-chat)