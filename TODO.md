#Apontamentos
     - Implementar um Agendador de tarefas (tirar o que está no logger e implementar em uma classe separada)
     - Implementar visualização gráfica de labels nas mensagens
     - Verificar se o verificador de atualizações está sendo executado duas vezes
     - Implementar link para abrir pasta dos arquivos enviados e recebidos
     - Quando usuário cancela o login o server quebra as conexoes (ver log 2)
     - Implementar notificação se o usuário está digitando
     - X Melhorar a formatação da data no view do chat (Classe TextVisualizer.java)
     - Implementar solicitação de aceite do arquivo
     - Fazer icone da bandeja apenas piscar no linux quando mensagem no linux ubuntu
     - Usuário está perdendo o foco quando alguém entrada ou sai do chat
     - Verificar na classe /chat_pandata/src/main/java/com/mageddo/chat/gui/DestacaUsuario.java se a variável de instância hitRow não
       terá sempre a mesma referência para que não seja necessário ficar pegar sempre
       com getListaNovasMensagens(table) no método checaDestaque e removeDestaque()
   
     - Log 2
        Este item gerou a seguinte exeção no server:
        Exception in thread "Thread-0" java.util.ConcurrentModificationException
        at java.util.AbstractList$Itr.checkForComodification(AbstractList.jav
        at java.util.AbstractList$Itr.next(AbstractList.java:343)
        at com.mageddo.chat.run.ChatServer$Handler.atualizarListaUsuarios(Cha
        at com.mageddo.chat.run.ChatServer$Handler.removeUsuario(ChatServer.j
        at com.mageddo.chat.run.ChatServer$Handler.run(ChatServer.java:105)

#0.2.5
    - Criação de arquivo de configuração padrão quando não existir
    - Implementação de limpeza de downloads e versões antigas do chat
    - Organização do código de atualização de update
    - Resolução do problema de permissões de execução do atalho do chat no linux

#0.2.4
    - Implementação de logs
    - Otimização do save de históricos criando um executor de tarefas dentro do logger

#0.2.3
    - Implementação de atalhos no Windows e no Linux

#0.2.2 
    - Melhoria na implementação de atualização automática

#0.2.1
    - Correção de bugs na atualização automática no windows

#0.2.0
	- Atualização automática implementada

#0.1.9
	 - Fixado bug de destaque de usuário
	 - Implmentado autosave a cada 5 min
	 - Notifica usuário quando é recebido um novo arquivo


#0.1.8
     Histórico implementado
     Envio de arquivos
 
    
#0.0.7
    Palavras quebram quando se chega no tamanho do campo de visualização - OK
    Palavras quebram quando se chega no tamanho do campo editor - OK 
    Tirar o prefixo "WRK7-" ou "WRK7_" quando houver - OK
    Verificar BUG de que as vezes falha ao tirar a marcação laranja do usuário - OK
    Colocar contador de usuários - OK 
    Envio de arquivos - PROXIMA
    Colocar um jMenu no lugar do botão - PROXIMA
    Bug do modal do JOptionPane na hora da mensagem "Login incorreto" - OK



#0.0.1 - RELEASE
    Histórico
    Colocada data e hora de envio
    Envio de arquivos em até 2MB

#0.0.1
    Montando estrutura para histórico - OK
    Montando estrutura para envio de arquivos - OK
    Colocada data e hora de envio - OK
    Até então impossível implementar sinal de ausência
    
    
#0.0.1
    Criar botão para histórico
    Criar botão para envio de arquivo
    Ver problema de clicar no usuário e não tirar cor laranja
    Ver campo de digitação está comendo o texto
